//
//  ExplorationViewModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 13/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest
import RxTests
import RxSwift

@testable import Frienji

class ExplorationViewModelTests: XCTestCase {

    func testRemoveCatchedFrienjiFromFrienjisAround() {
        // Given
        let viewModel = createViewModel()
        let catched = Frienji.fake()
        let withoutCatched = [Frienji.fake(), Frienji.fake()]
        let withCatched = withoutCatched.arrayByInserting(catched, atIndex: 1)
        viewModel.frienjisAround.value = withCatched

        // When
        viewModel.catched.onNext(catched)

        // Then
        XCTAssertEqual(withoutCatched, viewModel.frienjisAround.value)
    }

    func testRemoveRejectedFrienjiFromFrienjisAround() {
        // Given
        let viewModel = createViewModel()
        let rejected = Frienji.fake()
        let withoutRejected = [Frienji.fake(), Frienji.fake()]
        let withRejected = withoutRejected.arrayByInserting(rejected, atIndex: 1)
        viewModel.frienjisAround.value = withRejected

        // When
        viewModel.rejected.onNext(rejected)

        // Then
        XCTAssertEqual(withoutRejected, viewModel.frienjisAround.value)
    }

    func testRemoveBlockedFrienjiFromFrienjisAround() {
        // Given
        let viewModel = createViewModel()
        let blocked = Frienji.fake()
        let withoutBlocked = [Frienji.fake(), Frienji.fake()]
        let withBlocked = withoutBlocked.arrayByInserting(blocked, atIndex: 1)
        viewModel.frienjisAround.value = withBlocked

        // When
        viewModel.blocked.onNext(blocked)

        // Then
        XCTAssertEqual(withoutBlocked, viewModel.frienjisAround.value)
    }

    func testPresentPreviewModal() {
        // Given
        let input = PreviewInput()
        let viewModel = createViewModel(input)

        // Then
        let values = ["x1": Frienji.fake(), "x2": Frienji.fake()]
        XCTAssertEvents(
            inputValues: values, inputEvents: "-x1-x2-|", inputSubject: viewModel.previewTapped,
            outputValues: values, outputEvents: "-x1-x2--", outputObservable: input.loaded
        )
    }

    func testNavigateToMyWall() {
        // Given
        let input = WallInput()
        let settings = FakeSettings()
        let viewModel = createViewModel(input, settings: settings)

        // Then
        let inputValues = ["v": ()]
        let outputValues = ["x": settings.userFrienji!]
        XCTAssertEvents(
            inputValues: inputValues, inputEvents: "-v-v-|", inputSubject: viewModel.myWallTapped,
            outputValues: outputValues, outputEvents: "-x-x--", outputObservable: input.loaded
        )
    }

    func testSwitchDarkBackgroundOnWhenPresentingPreviewModal() {
        // Given
        let viewModel = createViewModel()

        // Then
        let inputValues = ["x1": Frienji.fake(), "x2": Frienji.fake()]
        let outputValues = ["t": true, "f": false]
        XCTAssertEvents(
            inputValues: inputValues, inputEvents: "--x1-x2-|", inputSubject: viewModel.previewTapped,
            outputValues: outputValues, outputEvents: "f-t--t---", outputObservable: viewModel.darkBackgroundVisible.asObservable()
        )
    }

    func testSwitchDarkBackgroundOffWhenDismissingPreviewModal() {
        // Given
        let viewModel = createViewModel()
        viewModel.darkBackgroundVisible.value = true

        // Then
        let inputValues = ["x1": Frienji.fake(), "x2": Frienji.fake()]
        let outputValues = ["t": true, "f": false]
        XCTAssertEvents(
            inputValues: inputValues, inputEvents: "--x1-x2-|", inputSubject: viewModel.catched,
            outputValues: outputValues, outputEvents: "t-f--f---", outputObservable: viewModel.darkBackgroundVisible.asObservable()
        )
    }

    func createViewModel(input: Input? = nil, settings: Settings = FakeSettings()) -> ExplorationViewModel {

        class FakeApi: FrienjiApi {

            override func saveFrienji(frienji: Frienji) -> Observable<Void> {
                return Observable.just()
            }

            override func rejectFrienji(frienji: Frienji) -> Observable<Void> {
                return Observable.just()
            }

            override func getFrienjisAround(latitude: Double, longitude: Double) -> Observable<[Frienji]> {
                return Observable.just([])
            }

            override func updateLocation(latitude: Double, longitude: Double) -> Observable<Frienji> {
                return Observable.just(Frienji.fake())
            }

        }

        return ExplorationViewModel(navigationHandler: FakeNavigationHandler(input: input), settings: settings, api: FakeApi())
    }

}
