//
//  FakeNavigationHandler.swift
//  Frienji
//
//  Created by Piotr Łyczba on 13/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
@testable import Frienji

class FakeNavigationHandler: NavigationHandler {

    let input: Input?

    var segueIdentifier: String!

    init(input: Input?) {
        self.input = input
        super.init(sourceViewController: nil)
    }

    override func navigate<DestinationType: HasInput>(segueIdentifier: String, destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        self.segueIdentifier = segueIdentifier
        if let input = input as? DestinationType.InputType {
            destinationInputHandler(input)
        }
    }

    override func dismissModal<DestinationType: HasInput>(destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        if let input = input as? DestinationType.InputType {
            destinationInputHandler(input)
        }
    }

}
