//
//  ChatViewModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest
@testable import Frienji

class ChatViewModelTests: XCTestCase {

    let sender = Frienji.fake()
    let receiver = Frienji.fake()
    let messages = [Message.fake()]

    var viewModel: ChatViewModel!

    override func setUp() {
        super.setUp()

        viewModel = ChatViewModel(sender: sender, receiver: receiver, messages: messages)
    }

    func testLoadMessages() {
        // Given:
        let messages = [Message.fake()]

        // When:
        let result = viewModel.executeCommand(ChatCommand.Load(messages: messages))

        // Then:
        XCTAssertEqual(sender, result.sender)
        XCTAssertEqual(receiver, result.receiver)
        XCTAssertEqual(messages, result.messages)
    }

    func testSendMessage() {
        // Given:
        let message = Message.fake()

        // When:
        let result = viewModel.executeCommand(ChatCommand.Send(message: message))

        // Then:
        XCTAssertEqual(sender, result.sender)
        XCTAssertEqual(receiver, result.receiver)
        XCTAssertTrue(result.messages.contains(message))
    }

    func testCreateViewModel() {
        XCTAssertEqual(sender, viewModel.sender)
        XCTAssertEqual(receiver, viewModel.receiver)
        XCTAssertEqual(messages, viewModel.messages)
    }

}
