//
//  PermissionsHelper.swift
//  Frienji
//
//  Created by bolek on 29.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation

class PermissionsHelper: NSObject, CLLocationManagerDelegate {
    
    var locationManager:CLLocationManager!
    private var handler: ((Bool)->Void)?
    
    override init() {
        super.init()
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = CLActivityType.Fitness
        self.locationManager.pausesLocationUpdatesAutomatically = true
        self.locationManager.distanceFilter = 100
        self.locationManager.delegate = self
    }
    
    static func openAppSettings() {
        if let url = NSURL(string: UIApplicationOpenSettingsURLString) where UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    //MARK: - Camera
    
    static func hasGrantedCameraPermission() -> Bool {
        return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == AVAuthorizationStatus.Authorized || AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == AVAuthorizationStatus.Restricted
    }
    
    static func askForCameraPermission(handler:(Bool)->Void) {
        AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) {
            (granded) in
                handler(granded)
        }
    }
    
    //MARK: - Push Notifications
    
    static func isRegisterdForPushNotifications() -> Bool {
        return UIApplication.sharedApplication().isRegisteredForRemoteNotifications()
    }
    
    static func registerForPushNotifications() {
        let userNotificationTypes : UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound])
        let userNotificationSettings: UIUserNotificationSettings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(userNotificationSettings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    //MARK: - Location
    
    static func hasGrantedLocationPermission() -> Bool {
        return CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedAlways || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.AuthorizedWhenInUse
    }
    
    func askForLocationAlwaysAuthorization(handler:(Bool)->()) {
        self.handler = handler
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if let handler = self.handler {
            handler(PermissionsHelper.hasGrantedLocationPermission())
        }
    }
}
