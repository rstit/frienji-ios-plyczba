//
//  HasDisposeBag.swift
//  Frienji
//
//  Created by Piotr Łyczba on 22/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

protocol HasDisposeBag {

    var disposeBag: DisposeBag { get }

}
