//
//  Settings.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Runes
import RxSwift

class Settings {

    // MARK: Shared instance

    static let sharedInstance = Settings()

    // MARK: Defaults Keys

    struct DefaultsKeys {
        static let PushNotificationsToken = "PushNotificationsTokenKey"
        static let RegisteredForPushNotifications = "RegisteredForPushNotificationsKey"
        static let IsUserVerfied = "IsUserVerfiedKey"
        static let UserId = "UserIdKey"
        static let UserAuthorizationToken = "UserAuthorizationTokenKey"
        static let UserAvatarGroup = "UserAvatarGroupKey"
        static let UserAvatarName = "UserAvatarNameKey"
        static let UsernameName = "UsernameKey"
        static let WhoAreYou = "WhoAreYouKey"
        static let UserCountryCode = "UserCountryCodeKey"
        static let UserCountryPhoneCode = "UserCountryPhoneCodeKey"
        static let UserCountryName = "UserCountryNameKey"
        static let UserPhoneNumber = "UserPhoneNumberKey"
        static let SessionToken = "SessionTokenKey"
        static let SessionClient = "SessionClient"
        static let SessionUID = "SessionUID"
        static let UserData = "UserData"
        static let CoachMarkShown = "CoachMarkShown"
    }

    // MARK: Properties

    var defaults: NSUserDefaults

    // MARK: Init

    init() {
        defaults = NSUserDefaults.standardUserDefaults()
    }

    // MARK: Settings

    var pushNotificationsToken: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.PushNotificationsToken) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.PushNotificationsToken)
        }
    }

    var registeredForPushNotifications: Bool {
        get {
            return defaults.objectForKey(DefaultsKeys.RegisteredForPushNotifications) as? Bool ?? false
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.RegisteredForPushNotifications)
        }
    }

    var userVerified: Bool {
        get {
            return defaults.objectForKey(DefaultsKeys.IsUserVerfied) as? Bool ?? false
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.IsUserVerfied)
        }
    }

    var userId: Int? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserId) as? Int
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserId)
        }
    }

    var userAuthorizationToken: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserAuthorizationToken) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserAuthorizationToken)
        }
    }

    var userAvatarGroup: AvatarType? {
        get {
            guard let value = defaults.objectForKey(DefaultsKeys.UserAvatarGroup) as? Int else {
                return nil
            }

            return AvatarType(rawValue: value)
        }
        set {
            defaults.setObject(newValue?.rawValue, forKey: DefaultsKeys.UserAvatarGroup)
        }
    }

    var userAvatarName: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserAvatarName) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserAvatarName)
        }
    }

    var username: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UsernameName) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UsernameName)
        }
    }

    var whoAreYou: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.WhoAreYou) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.WhoAreYou)
        }
    }

    var userCountryCode: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserCountryCode) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserCountryCode)
        }
    }

    var userCountryPhoneCode: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserCountryPhoneCode) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserCountryPhoneCode)
        }
    }

    var userCountryName: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserCountryName) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserCountryName)
        }
    }

    var userPhoneNumber: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.UserPhoneNumber) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.UserPhoneNumber)
        }
    }

    var sessionToken: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.SessionToken) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.SessionToken)
        }
    }

    var sessionClient: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.SessionClient) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.SessionClient)
        }
    }

    var sessionUID: String? {
        get {
            return defaults.objectForKey(DefaultsKeys.SessionUID) as? String
        }
        set {
            defaults.setObject(newValue, forKey: DefaultsKeys.SessionUID)
        }
    }

    var userData: AnyObject? {
        get {
            guard let data = defaults.objectForKey(DefaultsKeys.UserData) as? NSData else {
                return nil
            }
            return NSKeyedUnarchiver.unarchiveObjectWithData(data)
        }
        set {
            if let newValue = newValue {
                defaults.setObject(NSKeyedArchiver.archivedDataWithRootObject(newValue), forKey: DefaultsKeys.UserData)
            }
        }
    }

    var userFrienji: Frienji? {
        guard let userData = userData else {
            return nil
        }

        return decode(userData)
    }

    var rx_userFrienji: Observable<Frienji> {
        let decoded = userData.map(JSON.init).map(Frienji.decode) ?? .customError("No user data found in settings.")
        switch decoded {
        case let .Success(frienji):
            return .just(frienji)
        case let .Failure(error):
            return .error(error)
        }
    }

    var session: Session? {
        guard let token = sessionToken, client = sessionClient, uid = sessionUID else {
            return nil
        }

        return Session(token: token, client: client, uid: uid)
    }
    
    // MARK: Coach marks
    
    func wasCoachMarkShownForType(type:CoachMarkFactory.CoachMarkType) -> Bool {
        let key = DefaultsKeys.CoachMarkShown + "\(type.rawValue)"
        return defaults.objectForKey(key) as? Bool ?? false
    }
    
    func setCoachMarkAsShownForType(type:CoachMarkFactory.CoachMarkType) {
        let key = DefaultsKeys.CoachMarkShown + "\(type.rawValue)"
        defaults.setObject(true, forKey: key)
    }

}
