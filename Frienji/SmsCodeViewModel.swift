//
//  SmsCodeViewModel.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

class SmsCodeViewModel {

    var code: String!
    let kCodeLenght = 4

    let disposeBag = DisposeBag()

    func isCodeValid(completion: (isValid: Bool) -> Void) {
        completion(isValid: code.characters.count == kCodeLenght)
    }

    func validateSmsCode(controller: UIViewController, completion: (success: Bool) -> Void) {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)

        FrienjiApi.sharedInstance.signIn(code)
            .subscribe(
                onNext: { user in
                    Settings.sharedInstance.userId = user.userId
                    Settings.sharedInstance.username = user.username
                    Settings.sharedInstance.whoAreYou = user.whoAreYou
                    Settings.sharedInstance.userPhoneNumber = user.phoneNumber
                    Settings.sharedInstance.userAvatarName = user.avatar.name

                    activityView.hide()
                    completion(success: true)
                },
                onError: { error in
                    activityView.hide()
                    UIAlertUtils.showAlertWithTitle(FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
                    completion(success: false)
                }
            )
        .addDisposableTo(disposeBag)
    }

}
