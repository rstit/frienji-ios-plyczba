//
//  SignUpViewController.swift
//  Frienji
//
//  Created by bolek on 03.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignUpViewController: BaseViewController, StoryboardLoad
{
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "SignUpViewController"
    
    @IBOutlet var titleLbl :UILabel!
    @IBOutlet var infoLbl :UILabel!
    
    @IBOutlet var usernameTxt:UITextField!
    @IBOutlet var whoAreYouTxt:UITextField!
    
    private var viewModel:SignUpViewModel!
    private let disposeBag = DisposeBag()
    
    //MARK: - View lifecycle

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = SignUpViewModel()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.initControls()
        self.configBindings()
        self.configureTextFieldsPlaceholder()
        self.configureTextFieldsBottomBorder()
        self.configureTextFieldsInputAcessoryView()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.showKeyboard()
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }
    
    //MARK: - Private methods
    
    private func initControls()
    {
        self.titleLbl.text = Localizations.signUp.titleLbl
        self.infoLbl.text = Localizations.signUp.infoLbl
    }
    
    private func configBindings()
    {
        self.usernameTxt.rx_text.asObservable().bindNext { [unowned self](newUsername) in
            self.viewModel.username = newUsername
        }.addDisposableTo(self.disposeBag)
        
        self.whoAreYouTxt.rx_text.asObservable().bindNext { [unowned self](newWhoAreYou) in
            self.viewModel.whoAreYou = newWhoAreYou
        }.addDisposableTo(self.disposeBag)
    }
    
    private func configureTextFieldsPlaceholder()
    {
        self.usernameTxt.attributedPlaceholder = NSAttributedString(string:Localizations.signUp.usernameTxtPlaceholder,
                                                                     attributes:[NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor()])
        self.whoAreYouTxt.attributedPlaceholder = NSAttributedString(string:Localizations.signUp.whoAreYouTxtPlaceholder,
                                                                      attributes:[NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor()])
    }
    
    private func configureTextFieldsBottomBorder()
    {
        self.usernameTxt.setBottomBorder(UIColor.appOrangePlaceholderColor())
        self.whoAreYouTxt.setBottomBorder(UIColor.appOrangePlaceholderColor())
    }
    
    private func configureTextFieldsInputAcessoryView()
    {
        let keyboardInputAccessoryView = UIButton()
        keyboardInputAccessoryView.frame = CGRectMake(0, 0, self.view.frame.size.width,44)
        keyboardInputAccessoryView.setTitle(Localizations.signUp.nextBtn, forState: .Normal)
        keyboardInputAccessoryView.backgroundColor = UIColor.appLightBlueColor()
        keyboardInputAccessoryView.titleLabel?.font = UIFont.latoRegulatWithSize(17)
        keyboardInputAccessoryView.addTarget(self, action: #selector(nextClicked), forControlEvents: .TouchUpInside)
        
        self.usernameTxt.inputAccessoryView = keyboardInputAccessoryView
        self.whoAreYouTxt.inputAccessoryView = keyboardInputAccessoryView
    }
    
    private func showKeyboard()
    {
        self.usernameTxt.becomeFirstResponder()
    }
    
    private func checkUsernameAvailability()
    {
        self.dismissKeyboard()
        
        self.viewModel.checkUsernameAvailability(self) { (success, available) in
            if(success)
            {
                self.processSuccessResponse(available)
            }
        }
    }
    
    private func processSuccessResponse(available:Bool)
    {
        if(available)
        {
            self.viewModel.storeUserInfo()
            
            let viewCont = PhoneNumberViewController.loadFromStoryboard()
            viewCont.setEntryType(PhoneNumberViewEntryType.SignUp)
            self.navigationController?.pushViewController(viewCont, animated: true)
        }
        else
        {
            self.usernameTxt.becomeFirstResponder()
            UIAlertUtils.showAlertWithTitle(Localizations.signUp.usernameNotAvailableAlertTitle, message: Localizations.signUp.usernameNotAvailableAlertMessage, fromController: self, showCompletion: nil)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func goBackClicked(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func nextClicked(sender:UIButton)
    {
        if(self.usernameTxt.isFirstResponder())
        {
            self.whoAreYouTxt.becomeFirstResponder()
            return
        }
        self.viewModel.areFieldsValid { (areValid, isFirstValid, isSecondValid) in
            if(areValid)
            {
                self.checkUsernameAvailability()
                return
            }
            
            if(!isFirstValid)
            {
                UIView.shakeViewHorizontally(self.usernameTxt, times: 5, distance: 4)
            }
            
            if(!isSecondValid)
            {
                UIView.shakeViewHorizontally(self.whoAreYouTxt, times: 5, distance: 4)
            }
        }
    }
}
