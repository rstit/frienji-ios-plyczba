//
//  ZooViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 05/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import SwiftOrderedSet

struct ZooViewModel {

    let frienjis: [Frienji]
    let deletedFrienjisWithIndices: [(index: Int, frienji: Frienji)]

    var deletedFrienjis: [Frienji] {
        return deletedFrienjisWithIndices.map { $0.frienji }
    }

    init(frienjis: [Frienji] = [], deleted: [(index: Int, frienji: Frienji)] = []) {
        self.frienjis = frienjis
        self.deletedFrienjisWithIndices = deleted
    }

    func executeCommand(command: ZooCommand) -> ZooViewModel {
        switch command {
        case let .LoadFrienjis(newFrienjis):
            return ZooViewModel(frienjis: newFrienjis)
        case let .DeleteFrienjiAtIndexPath(index):
            let newFrienjis = frienjis.arrayByRemovingAtIndex(index)
            let deleted = frienjis[index]

            return ZooViewModel(frienjis: newFrienjis, deleted: [(index, deleted)] + deletedFrienjisWithIndices)
        case .CommitDelete:
            return ZooViewModel(frienjis: frienjis)
        case .CancelDelete:
            let newFrienjis = deletedFrienjisWithIndices.reduce(frienjis) { frienjis, deleted in
                frienjis.arrayByInserting(deleted.frienji, atIndex: deleted.index)
            }

            return ZooViewModel(frienjis: newFrienjis)
        }
    }

}

enum ZooCommand {
    case LoadFrienjis(frienjis: [Frienji])
    case DeleteFrienjiAtIndexPath(index: Int)
    case CommitDelete()
    case CancelDelete()
}
