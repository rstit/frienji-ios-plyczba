//
//  Message.swift
//  Frienji
//
//  Created by Piotr Łyczba on 16/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Message {

    let id: Int
    let content: String?
    let createdAt: NSDate
    let isAuthor: Bool
    let attachmentImageUrl: String?
    let latitude: Double?
    let longitude: Double?

}

// MARK: - Fake

extension Message: Fakeable {

    static func fake() -> Message {
        return Message(
            id: faker.number.increasingUniqueId(),
            content: faker.lorem.sentences(),
            createdAt: (NSDate.randomWithinDaysBeforeToday(30)),
            isAuthor: faker.number.randomBool(),
            attachmentImageUrl: faker.internet.image(),
            latitude: faker.address.latitude(),
            longitude: faker.address.longitude()
        )
    }

}

// MARK: - Decode

extension Message: Decodable {

    static func decode(json: JSON) -> Decoded<Message> {
        return curry(Message.init)
            <^> json <| "id"
            <*> json <|? "content"
            <*> (json <| "created_at" >>- toNSDate)
            <*> json <| "is_author" <|> pure(false)
            <*> json <|? "attachment_url"
            <*> json <|? ["location", "latitude"]
            <*> json <|? ["location", "longitude"]
    }

}

// MARK: - Equate

extension Message: Equatable {

}

func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.id == rhs.id
}
