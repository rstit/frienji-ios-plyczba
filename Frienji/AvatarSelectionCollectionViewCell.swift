//
//  AvatarSelectionCollectionViewCell.swift
//  Frienji
//
//  Created by bolek on 02.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AvatarSelectionCollectionViewCell: UICollectionViewCell
{
    static let identifier = "AvatarSelectionCollectionViewCellIdentifier"
    
    var avatarName:String!
    
    @IBOutlet weak var avatarImg:UIImageView!
    @IBOutlet weak var avatarLbl:UILabel!
    
    //MARK: - View lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let bgView = UIView()
        bgView.backgroundColor = UIColor.appLightGreenSelectedCellColor()
        self.selectedBackgroundView = bgView
    }
    
    //MARK: - Public methods
    
    func configureCellFor(avatarName:String, entryType: AvatarType)
    {
        self.avatarName = avatarName
            
        self.avatarImg.hidden = entryType == AvatarType.Emoji
        self.avatarLbl.hidden = entryType != AvatarType.Emoji
        
        switch entryType
        {
        case AvatarType.Emoji:
            self.avatarLbl.text = self.avatarName
            break
        case AvatarType.Image:
            self.avatarImg.image = UIImage(imageLiteral: self.avatarName)
            break
        case AvatarType.AnimatedImage:
            self.avatarImg.image = UIImage.getAnimatedImageFrom(self.avatarName)
            break
        }
    }
}
