//
//  AnimatorPush.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AnimatorPush: NSObject, UIViewControllerAnimatedTransitioning {

    private let kDuration :Double = 0.2
    var isPresenting :Bool = false
    
    //MARK: - UIViewControllerAnimatedTransitioning
    
    func transitionDuration(transitionContext :UIViewControllerContextTransitioning?) -> NSTimeInterval
    {
        return self.kDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        if(self.isPresenting == true)
        {
            self.pushAnimation(transitionContext)
        }
        else
        {
            self.popAnimation(transitionContext)
        }
    }
    
    //MARK: - Private methods
    
    private func pushAnimation(transitionContext :UIViewControllerContextTransitioning)
    {
        guard
            let inView :UIView = transitionContext.containerView(),
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey),
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
            else {return}

        fromViewController.view.frame = inView.bounds
        toViewController.view.frame = inView.bounds
        
        inView.addSubview(fromViewController.view)
        inView.addSubview(toViewController.view)
        
        let toViewControllerActual : UIViewController?
        if let navigationController = toViewController as? UINavigationController {
            toViewControllerActual = navigationController.topViewController
        }
        else {
            toViewControllerActual = toViewController
        }
        
        let fromViewControllerActual : UIViewController?
        if let navigationController = fromViewController as? UINavigationController {
            fromViewControllerActual = navigationController.topViewController
        }
        else {
            fromViewControllerActual = fromViewController
        }
        
        if(self.shouldPushTowardLeft(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext)
        }
        else if(self.shouldPushTowardRight(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext)
        }
        else if(self.shouldPresentModal(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.showModalFromViewController(fromViewController, toViewController: toViewController, transitionContext: transitionContext)
        }
        else
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext)
        }
    }
    
    private func popAnimation(transitionContext :UIViewControllerContextTransitioning)
    {
        guard
            let inView :UIView = transitionContext.containerView(),
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey),
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
            else {return}
        
        inView.backgroundColor = UIColor.clearColor()
        
        fromViewController.view.frame = inView.bounds
        toViewController.view.frame = inView.bounds
        
        inView.addSubview(fromViewController.view)
        inView.addSubview(toViewController.view)
        
        let toViewControllerActual : UIViewController?
        if let navigationController = toViewController as? UINavigationController {
            toViewControllerActual = navigationController.topViewController
        }
        else {
            toViewControllerActual = toViewController
        }
        
        let fromViewControllerActual : UIViewController?
        if let navigationController = fromViewController as? UINavigationController {
            fromViewControllerActual = navigationController.topViewController
        }
        else {
            fromViewControllerActual = fromViewController
        }
        
        
        if(self.shouldPopTowardRight(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext)
        }
        else if(self.shouldPopTowardLeft(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: true, transitionContext: transitionContext)
        }
        else if(self.shouldDismissModal(fromViewControllerActual, toViewController: toViewControllerActual))
        {
            self.dismissModalFromViewController(fromViewController, toViewController: toViewController, transitionContext: transitionContext)
        }
        else
        {
            self.animateFromViewController(fromViewController, toViewController: toViewController, towardLeft: false, transitionContext: transitionContext)
        }
    }
    
    private func dismissModalFromViewController(fromViewController :UIViewController, toViewController :UIViewController, transitionContext :UIViewControllerContextTransitioning)
    {
        guard
            let inView :UIView = transitionContext.containerView()
            else {return}
        
        inView.bringSubviewToFront(fromViewController.view)
        
        UIView.animateWithDuration(self.kDuration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            fromViewController.view.frame = CGRectMake(0.0, inView.frame.size.height, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height)
            }) { (finished) in
                transitionContext.completeTransition(true)
        }
    }
    
    private func showModalFromViewController(fromViewController :UIViewController, toViewController :UIViewController, transitionContext :UIViewControllerContextTransitioning)
    {
        guard
            let inView :UIView = transitionContext.containerView()
            else {return}
        
        toViewController.view.frame = CGRectMake(0.0, inView.frame.size.height, toViewController.view.frame.size.width, toViewController.view.frame.size.height)
        
        UIView.animateWithDuration(self.kDuration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            toViewController.view.frame = inView.bounds
            }) { (finished) in
                transitionContext.completeTransition(true)
        }
    }
    
    private func animateFromViewController(fromViewController :UIViewController, toViewController :UIViewController, towardLeft :Bool, transitionContext :UIViewControllerContextTransitioning)
    {
        let toViewOriginalFrame = toViewController.view.frame
        if(towardLeft)
        {
            toViewController.view.frame = CGRectMake(toViewController.view.frame.width, 0.0, toViewController.view.frame.size.width, toViewController.view.frame.size.height)
        }
        else
        {
            toViewController.view.frame = CGRectMake(-toViewController.view.frame.width, 0.0, toViewController.view.frame.size.width, toViewController.view.frame.size.height)
        }
        
        UIView.animateWithDuration(self.kDuration, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
            toViewController.view.frame = toViewOriginalFrame
            if(towardLeft)
            {
                fromViewController.view.frame = CGRectMake(0, 0.0, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height)
            }
            else
            {
                fromViewController.view.frame = CGRectMake(0, 0.0, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height)
            }
        }) { (finished) in
            transitionContext.completeTransition(true)
        }
    }
    
    private func shouldPushTowardLeft(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - handle further views so that they all arrive from the same
        return fromViewController is ZooViewController && toViewController is ArViewController
    }
    
    private func shouldPushTowardRight(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - it wont be newsfeed, probobly it will be profile, handle further views so that they all arrive from the same
        return fromViewController is ArViewController && toViewController is WallViewController
    }
    
    private func shouldPresentModal(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - handle some animations as Modal - that is from bottom
        return false
    }
    
    private func shouldPopTowardLeft(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - handle other transitions like fin push method
        return fromViewController is WallViewController && toViewController is ArViewController
    }
    
    private func shouldPopTowardRight(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - handle further views so that they all arrive from the same
        return fromViewController is ZooViewController && toViewController is ArViewController
    }
    
    private func shouldDismissModal(fromViewController:UIViewController?, toViewController:UIViewController?)->Bool
    {
        //TODO: - handle some dismissing animations as Modal - that is from bottom
        return false
    }
}
