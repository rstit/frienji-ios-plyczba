//
//  PhoneNumberViewModel.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import libPhoneNumber_iOS

enum PhoneNumberViewEntryType :Int
{
    case SignUp = 0
    case SignIn = 1
}

class PhoneNumberViewModel
{
    var entryType : PhoneNumberViewEntryType!
    var wasRegistractionSuccessfull:Bool = false
    
    var phoneNumber:String!
    var countryCode:String!
    var countryPhoneCode:Int!
    var countryName:String!
    var formatedPhoneNumber:String!
    
    let disposeBag = DisposeBag()
    
    func getViewTitle() -> String
    {
        if(self.entryType == PhoneNumberViewEntryType.SignUp)
        {
            return Localizations.phoneNumber.titleLblSignUp
        }
        else
        {
            return Localizations.phoneNumber.titleLblSignIn
        }
    }
    
    func isPhoneNumberValid( completion:(isValid:Bool)->Void)
    {
        PhoneNumberUtils.validatePhonuNumber(self.phoneNumber, countryCode: self.countryCode, countryPhoneCode: self.countryPhoneCode)
        { (isValid, formatedPhoneNumber) in
            if let formatedPhoneNumberUnwrapped = formatedPhoneNumber {
                self.formatedPhoneNumber = formatedPhoneNumberUnwrapped
            }
            completion(isValid: isValid)
        }
    }
    
    func updateCoutryCode(ISOCountryCode:String) -> String
    {
        self.countryCode = ISOCountryCode
        self.countryPhoneCode = NBPhoneNumberUtil.sharedInstance().getCountryCodeForRegion(ISOCountryCode).integerValue
        
        self.countryName = NSLocale.currentLocale().displayNameForKey(NSLocaleCountryCode, value: ISOCountryCode) as String!
        
        return Localizations.phoneNumber.contryCode(self.countryPhoneCode!, self.countryName!)
    }
    
    func getTitleForCoutryCodePickerRow(row:Int)->String
    {
        let countryCode = NSLocale.ISOCountryCodes()[row]
        let countryPhoneCode = NBPhoneNumberUtil.sharedInstance().getCountryCodeForRegion(countryCode)
        let countryName = NSLocale.currentLocale().displayNameForKey(NSLocaleCountryCode, value: countryCode)
        
        return "+\(countryPhoneCode) - \(countryName!)"
    }
    
    var getNumberOfComponentsForCoutryCodePicker : Int {
        get {
            return 1
        }
    }
    
    func getNumberOfRowsForCoutryCodePicker(component:Int) -> Int
    {
        return NSLocale.ISOCountryCodes().count
    }
    
    func getDefaultCoutryCodeBasedOnLocale() ->Int
    {
        var countryCode: String? = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String
        if(countryCode == nil)
        {
            countryCode = NSLocale(localeIdentifier: NSLocale.preferredLanguages().first!).objectForKey(NSLocaleCountryCode) as? String
        }
        
        let range = NSLocale.ISOCountryCodes().count - 1
        for i in 0...range
        {
            if(NSLocale.ISOCountryCodes()[i] == countryCode!)
            {
                self.updateCoutryCode(countryCode!)
                return i
            }
        }
        return 0
    }
    
    func storeUserInfo()
    {
        Settings.sharedInstance.userCountryCode = self.countryCode
        Settings.sharedInstance.userCountryPhoneCode = "+\(self.countryPhoneCode)"
        Settings.sharedInstance.userCountryName = self.countryName
        Settings.sharedInstance.userPhoneNumber = self.phoneNumber.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    
    func createAccountAndSendSmsCode(controller:UIViewController, completion:(success:Bool)->Void)
    {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)
        
        FrienjiApi.sharedInstance.signUp()
            .subscribe(
                onNext: { accountCreated in
                    self.wasRegistractionSuccessfull = accountCreated
                    activityView.hide()
                    completion(success: true)
                },
                onError: { error in
                    activityView.hide()
                    UIAlertUtils.showAlertWithTitle(FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
                    completion(success: false)
                }
        )
        .addDisposableTo(disposeBag)
    }
    
    func sendSmsCode(controller:UIViewController, completion:(success:Bool)->Void)
    {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)
        
        FrienjiApi.sharedInstance.sendLoginCode()
            .subscribe(
                onNext: { //[activityView, completion] in
                    activityView.hide()
                    completion(success: true)
                },
                onError: { error in
                    activityView.hide()
                    UIAlertUtils.showAlertWithTitle(FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
                    completion(success: false)
                }
        )
        .addDisposableTo(disposeBag)
    }
}