//
//  InboxViewCell.swift
//  Frienji
//
//  Created by Piotr Łyczba on 16/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class InboxTableViewCell: UITableViewCell {

    static let identifier = "inboxCell"

    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var unreadFlag: UILabel!

    let content = PublishSubject<Conversation>()
    let markedAsRead = PublishSubject<Void>()
    let disposeBag = DisposeBag()

    override func didMoveToSuperview() {
        // Initial load
        let loadCommand = content.map(InboxCellCommand.Load)

        // Mark as read
        let markReadCommand = markedAsRead
            .map { InboxCellCommand.MarkAsRead }

        // View model
        let viewModel = Observable.of(
            loadCommand,
            markReadCommand
            )
            .merge()
            .scan(InboxCellModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)

        // Bind view model
        viewModel.map { $0.authorName }.bindTo(authorName.rx_text).addDisposableTo(disposeBag)
        viewModel.map { $0.createdAt }.bindTo(createdAt.rx_text).addDisposableTo(disposeBag)
        viewModel.map { $0.details }.bindTo(details.rx_text).addDisposableTo(disposeBag)
        viewModel.map { $0.avatar }.bindTo(avatar.rx_image).addDisposableTo(disposeBag)
        viewModel.map { $0.isRead }.filter { $0 }.subscribeNext { [unowned self] _ in
            self.backgroundColor = UIColor.clearColor()
            self.unreadFlag.hidden = true
            }
            .addDisposableTo(disposeBag)
    }

}
