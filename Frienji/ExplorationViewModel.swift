//
//  MutableExplorationViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 11/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Argo

let kLocationChangeTimeInterval = 2.0

class ExplorationViewModel {

    // MARK: - Output variables

    var frienjisAround = Variable<[Frienji]>([])
    var darkBackgroundVisible = Variable<Bool>(false)

    // MARK: - Inputs

    let catched = PublishSubject<Frienji>()
    let rejected = PublishSubject<Frienji>()
    let blocked = PublishSubject<Frienji>()

    let previewTapped = PublishSubject<Frienji>()
    let myWallTapped = PublishSubject<Void>()
    let activitiesTapped = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    private let navigationHandler: NavigationHandler
    private let settings: Settings
    private let api: FrienjiApi
    private let locationManager: LocationManager
    private let notificationHandler: NotificationHandler

    // MARK: - Dependencies

    init(
        navigationHandler: NavigationHandler,
        settings: Settings = Settings.sharedInstance,
        api: FrienjiApi = FrienjiApi.sharedInstance,
        locationManager: LocationManager = LocationManager.sharedInstance,
        notificationHandler: NotificationHandler = NotificationHandler()
        ) {
        self.navigationHandler = navigationHandler
        self.settings = settings
        self.api = api
        self.locationManager = locationManager
        self.notificationHandler = notificationHandler

        prepareLoadFrienjisAround()
        prepareUpdateLocation()
        prepareRemoveFromFrienjisAround()
        prepareSwitchTranluscientBackground()
        prepareNavigation()
    }

    private func prepareLoadFrienjisAround() {
        locationManager.currentLocation.asObservable()
            .throttle(kLocationChangeTimeInterval, scheduler: MainScheduler.instance)
            .flatMap { [unowned self] location -> Observable<[Frienji]> in
                guard let location = location else {
                    return Observable.empty()
                }

                return self.api.getFrienjisAround(location.coordinate.latitude, longitude: location.coordinate.longitude)
            }
            .map { $0.filter { $0.relation != .Blocked} }
            .bindTo(frienjisAround)
            .addDisposableTo(disposeBag)
    }

    private func prepareUpdateLocation() {
        locationManager.currentLocation.asObservable()
            .throttle(kLocationChangeTimeInterval, scheduler: MainScheduler.instance)
            .flatMap { location -> Observable<Frienji> in
                guard let location = location else {
                    return Observable.empty()
                }

                return self.api.updateLocation(location.coordinate.latitude, longitude: location.coordinate.longitude)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    private func prepareRemoveFromFrienjisAround() {
        let catched = self.catched
            .flatMap(api.saveFrienji)
            .withLatestFrom(self.catched)
        let rejected = self.rejected
            .flatMap(api.rejectFrienji)
            .withLatestFrom(self.rejected)

        Observable.of(catched, rejected, blocked).merge()
            .withLatestFrom(frienjisAround.asObservable()) { (frienji, frienjis) in
                frienjis.filter { $0 != frienji }
            }
            .bindTo(frienjisAround)
            .addDisposableTo(disposeBag)
    }

    private func prepareSwitchTranluscientBackground() {
        previewTapped
            .map { _ in true }
            .bindTo(darkBackgroundVisible)
            .addDisposableTo(disposeBag)
        Observable.of(self.catched, self.rejected).merge()
            .map { _ in false }
            .bindTo(darkBackgroundVisible)
            .addDisposableTo(disposeBag)
    }

    private func prepareNavigation() {
        previewTapped
            .bindTo(navigationHandler.segue(R.segue.explorationViewController.showPreview.identifier, destinationType: PreviewViewController.self)) { input, frienji in
                input.loaded.onNext(frienji)
            }
            .addDisposableTo(disposeBag)
        myWallTapped
            .flatMap { [unowned self] in
                self.settings.rx_userFrienji
            }
            .bindTo(navigationHandler.segue(R.segue.explorationViewController.showMyWall.identifier, destinationType: WallViewController.self)) { input, user in
                input.loaded.onNext(user)
            }
            .addDisposableTo(disposeBag)
        let notificationTapped = notificationHandler.notificationTapped.map { _ in }
        Observable.of(activitiesTapped, notificationTapped).merge()
            .bindTo(navigationHandler.segue(R.segue.explorationViewController.showActivities.identifier, destinationType: ActivitiesViewController.self))
            .addDisposableTo(disposeBag)
    }

}
