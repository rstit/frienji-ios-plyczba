//
//  TracesApi.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import RxSwift
import Alamofire
import RxAlamofire
import Argo
import Runes

class FrienjiApi {

    enum Endpoint {

        case UsernameAvailability
        case Frienjis
        case SendLoginCode
        case SignIn
        case ProfileFrienjis
        case Conversations
        case Messages(conversationId: Int)
        case MessagesSend(conversationId: Int)
        case FrienjiPosts(frienjiId: Int)
        case FrienjiPost(frienjiId: Int, postId: Int)
        case FrienjiPostComments(frienjiId: Int, postId: Int)
        case Post(postId: Int)
        case PostLike(postId: Int)
        case FrienjiSave(frienjiId: Int)
        case FrienjiReject(frienjiId: Int)
        case FrienjisBulkReject
        case Profile
        case Notifications

        var urlPart: String {
            switch self {
            case .UsernameAvailability:
                return "frienjis/check_username_availability"
            case .Frienjis:
                return "frienjis"
            case .SendLoginCode:
                return "frienjis/send_login_code"
            case .SignIn:
                return "frienjis/sign_in"
            case .ProfileFrienjis:
                return "profile/frienjis"
            case .Conversations:
                return "conversations"
            case let .Messages(conversationId):
                return "conversations/\(conversationId)"
            case let .MessagesSend(conversationId):
                return "conversations/\(conversationId)/messages"
            case let .FrienjiPosts(frienjiId):
                return "frienjis/\(frienjiId)/posts"
            case let .FrienjiPost(frienjiId, postId):
                return "frienjis/\(frienjiId)/posts/\(postId)"
            case let .FrienjiPostComments(frienjiId, postId):
                return "frienjis/\(frienjiId)/posts/\(postId)/comments"
            case let .Post(postId):
                return "posts/\(postId)"
            case let .PostLike(postId):
                return "posts/\(postId)/likes"
            case let .FrienjiSave(frienjiId):
                return "frienjis/\(frienjiId)/saves"
            case let .FrienjiReject(frienjiId):
                return "frienjis/\(frienjiId)/rejects"
            case .FrienjisBulkReject:
                return "frienjis/reject_bulks"
            case .Profile:
                return "profile"
            case .Notifications:
                return "profile/notifications"
            }
        }

        var requestUrl: String {
            let baseURL = ConfigurationsHelper.sharedInstance.getBackendUrl()

            return baseURL + "/\(FrienjiApi.ApiVersion)/\(urlPart)"
        }

    }

    // MARK: Shared Instance

    static let sharedInstance = FrienjiApi()
    static let jsonDateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = FrienjiApi.ApiDateFormat
        return dateFormatter
    }()

    // MARK: Properties

    static let ApiVersion = "v1"
    static let RetryCount = 3
    static let ApiDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"

    let settings = Settings.sharedInstance
    let activityIndicator = ActivityIndicator()
    let reachabilityService = try? DefaultReachabilityService()

    // MARK: General request method

    func performRequest(method: Alamofire.Method, endpoint: FrienjiApi.Endpoint, parameters: [String: AnyObject]? = nil, encoding: ParameterEncoding = .URL, headers: [String: String]? = nil) -> Observable<AnyObject> {
        return request(method, endpoint.requestUrl, parameters: parameters, encoding: encoding, headers: headers)
            .flatMap {
                $0
                    .validate(statusCode: 200 ..< 300)
                    .validate(contentType: ["application/json"])
                    .rx_JSON()
            }
            .retry(FrienjiApi.RetryCount)
            .trackActivity(activityIndicator)
            .doOnError { print($0) }
    }

    // MARK: Helpers

    private func createAuthorizationHeaders() -> [String: String] {
        guard let token = settings.sessionToken, client = settings.sessionClient, uid = settings.sessionUID else {
            return [String: String]()
        }

        return ["token": token, "client": client, "uid": uid]
    }

    // MARK: - Username Availability

    func checkUserNameAvailability(username: String) -> Observable<Bool> {
        let parameters = ["username": username]

        return performRequest(.GET, endpoint: .UsernameAvailability, parameters: parameters)
            .retryOnBecomesReachable(reachabilityService)
            .decode { $0 <| "username_available" }
            .observeOn(MainScheduler.instance)
    }

    // MARK: - Sign up

    func signUp() -> Observable<Bool> {
        let avatarParams = [
            "avatar_name": settings.userAvatarName ?? "",
            "avatar_type": settings.userAvatarGroup?.rawValue ?? AvatarType.Emoji.rawValue
        ]
        let params: [String: AnyObject] = [
            "avatar_name": settings.userAvatarName ?? "",
            "avatar_attributes": avatarParams,
            "country_code": settings.userCountryPhoneCode ?? "",
            "username": settings.username ?? "",
            "phone_number": settings.userPhoneNumber ?? "",
            "who_you_are": settings.whoAreYou ?? "",
        ]

        return performRequest(.POST, endpoint: .Frienjis, parameters: params, encoding: .JSON, headers: nil)
            .retryOnBecomesReachable(reachabilityService)
            .decode { $0 <| "status" }
            .map { (status: String) in status == "success" }
            .observeOn(MainScheduler.instance)

    }

    // MARK: - Sign in

    func sendLoginCode() -> Observable<Void> {

        let params = [
            "country_code": settings.userCountryPhoneCode ?? "",
            "phone_number": settings.userPhoneNumber ?? "",
            ]

        return performRequest(.POST, endpoint: .SendLoginCode, parameters: params, encoding: .JSON, headers: nil)
            .retryOnBecomesReachable(reachabilityService)
            .map { _ in return }
            .observeOn(MainScheduler.instance)
    }

    func signIn(smsCode: String) -> Observable<Frienji> {

        let params: [String: AnyObject] = [
            "country_code": settings.userCountryPhoneCode ?? "",
            "phone_number": settings.userPhoneNumber ?? "",
            "password": smsCode,
        ] + createNotificationParameters()

        let response = performRequest(.POST, endpoint: .SignIn, parameters: params, encoding: .JSON, headers: nil)
            .retryOnBecomesReachable(reachabilityService)
            .shareReplay(1)

        let token: Observable<Session> = response
            .decode()
            .doOnNext { [unowned self] session in
                self.settings.sessionToken = session.token
                self.settings.sessionClient = session.client
                self.settings.sessionUID = session.uid
                self.settings.userVerified = true
            }

        let user: Observable<Frienji> = response
            .decode { $0 <| "data" }
            .withLatestFrom(response) { (user: Frienji, data) in
                self.settings.userData = data["data"]

                return user
            }

        return token.withLatestFrom(user)
            .observeOn(MainScheduler.instance)
    }

    func logout() -> Observable<Void> {
        return Observable.create { [unowned self] observer in
            self.settings.sessionToken = nil
            self.settings.sessionClient = nil
            self.settings.sessionUID = nil
            self.settings.userVerified = false

            observer.onNext()
            observer.onCompleted()

            return NopDisposable.instance
        }
    }

    // MARK: - Zoo

    func getFrienjis(loadMore: Observable<Void>) -> Observable<[Frienji]> {
        return getPaginated(.ProfileFrienjis, loadMoreTrigger: loadMore) { $0 <|| "data" }
    }

    func deleteFrienjis(frienjis: [Frienji]) -> Observable<Void> {
        return performRequest(.POST, endpoint: .FrienjisBulkReject, parameters: ["ids": frienjis.map { $0.userId }], encoding: .JSON, headers: createAuthorizationHeaders())
            .map { _ in () }
            .observeOn(MainScheduler.instance)
    }

    // MARK: - Messages

    func getConversations(loadMore: Observable<Void>?) -> Observable<[Conversation]> {
        return getPaginated(.Conversations, loadMoreTrigger: loadMore) { $0 <|| "data" }
    }

    func createConversation(frienji: Frienji) -> Observable<Conversation> {
        return performRequest(.POST,
            endpoint: .Conversations,
            parameters: ["recipient_id": frienji.userId],
            encoding: .JSON,
            headers: createAuthorizationHeaders()
            )
            .decode()
            .observeOn(MainScheduler.instance)
    }

    func getConversationMessages(conversation: Conversation, loadMore: Observable<Void>?) -> Observable<[Message]> {
        return getPaginatedReversed(.Messages(conversationId: conversation.id), loadMoreTrigger: loadMore) { $0 <|| "data" }
    }

    func sendMessage(conversation: Conversation, content: String?, image: UIImage?, latitude: Double?, longitude: Double?) -> Observable<Message> {
        return performMultipartUploadRequest(.POST,
            endpoint: .MessagesSend(conversationId: conversation.id),
            headers: createAuthorizationHeaders()) { multipartFormData in
                multipartFormData.appendText(content, key: "content")
                multipartFormData.appendImage(image, key: "attachment")
                multipartFormData.appendLocation(latitude, longitude: longitude)
            }
            .decode()
            .observeOn(MainScheduler.instance)
    }

    // MARK: - Wall

    func getFrienjisPosts(frienji: Frienji, loadMore: Observable<Void>) -> Observable<[Post]> {
        return getPaginated(.FrienjiPosts(frienjiId: frienji.userId), loadMoreTrigger: loadMore) { $0 <|| "data" }
    }

    func getPostComments(frienji: Frienji, post: Post, loadMore: Observable<Void>) -> Observable<[Post]> {
        return getPaginated(.FrienjiPost(frienjiId: frienji.userId, postId: post.id), loadMoreTrigger: loadMore) { $0 <|| ["data", "comments"] }
    }

    func sendPost(frienji: Frienji, message: String) -> Observable<Post> {
        return performRequest(.POST,
                endpoint: .FrienjiPosts(frienjiId: frienji.userId),
                parameters: ["message": message],
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .decode()
            .observeOn(MainScheduler.instance)
    }

    func sendPost(frienji: Frienji, message: String?, image: UIImage?, latitude: Double?, longitude: Double?) -> Observable<Post> {
        return performMultipartUploadRequest(.POST,
            endpoint: .FrienjiPosts(frienjiId: frienji.userId),
            headers: createAuthorizationHeaders()) { multipartFormData in
                multipartFormData.appendText(message, key: "message")
                multipartFormData.appendImage(image, key: "attachment")
                multipartFormData.appendLocation(latitude, longitude: longitude)
            }
            .decode()
            .observeOn(MainScheduler.instance)
    }

    func sendComment(frienji: Frienji, post: Post, message: String) -> Observable<Post> {
        return performRequest(.POST,
                endpoint: .FrienjiPostComments(frienjiId: frienji.userId, postId: post.id),
                parameters: ["message": message],
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .decode()
            .observeOn(MainScheduler.instance)
    }

    func sendComment(frienji: Frienji, post: Post, message: String?, image: UIImage?, latitude: Double?, longitude: Double?) -> Observable<Post> {
        return performMultipartUploadRequest(.POST,
            endpoint: .FrienjiPostComments(frienjiId: frienji.userId, postId: post.id),
            headers: createAuthorizationHeaders()) { multipartFormData in
                multipartFormData.appendText(message, key: "message")
                multipartFormData.appendImage(image, key: "attachment")
                multipartFormData.appendLocation(latitude, longitude: longitude)
            }
            .decode()
            .observeOn(MainScheduler.instance)
    }

    func likePost(post: Post) -> Observable<Void> {
        return performRequest(.POST,
                endpoint: .PostLike(postId: post.id),
                parameters: nil,
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .map { _ in () }
            .observeOn(MainScheduler.instance)
    }

    func deletePost(post: Post) -> Observable<Void> {
        return performRequest(.DELETE,
                endpoint: .Post(postId: post.id),
                parameters: nil,
                encoding: .URL,
                headers: createAuthorizationHeaders()
            )
            .map { _ in () }
            .observeOn(MainScheduler.instance)
    }

    // MARK: - Exploration

    func getFrienjisAround(latitude: Double, longitude: Double) -> Observable<[Frienji]> {
        return performRequest(.GET,
                endpoint: .Frienjis,
                parameters: ["location": ["latitude": latitude, "longitude": longitude]],
                encoding: .URL,
                headers: createAuthorizationHeaders()
            )
            .decode { json -> Decoded<[Frienji]> in decodeArray(json) }
            .observeOn(MainScheduler.instance)
    }

    func saveFrienji(frienji: Frienji) -> Observable<Void> {
        return performRequest(.POST,
                endpoint: .FrienjiSave(frienjiId: frienji.userId),
                parameters: nil,
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .map { _ in }
            .observeOn(MainScheduler.instance)
    }

    func rejectFrienji(frienji: Frienji) -> Observable<Void> {
        return performRequest(.POST,
                endpoint: .FrienjiReject(frienjiId: frienji.userId),
                parameters: nil,
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .map { _ in }
            .observeOn(MainScheduler.instance)
    }

    func updateLocation(latitude: Double, longitude: Double) -> Observable<Frienji> {
        return performRequest(.PATCH,
                endpoint: .Profile,
                parameters: ["location": ["latitude": latitude, "longitude": longitude]],
                encoding: .JSON,
                headers: createAuthorizationHeaders()
            )
            .decode()
            .observeOn(MainScheduler.instance)
    }

    // MARK: - Activities

    func getActivities(loadMore: Observable<Void>? = nil) -> Observable<[Activity<Any>]> {
        return getPaginated(.Notifications, loadMoreTrigger: loadMore) { $0 <|| "data" }
    }

    // MARK: - Settings

    func updateProfile(name: String? = nil, bio: String? = nil, avatar: Avatar? = nil, cover: UIImage? = nil) -> Observable<Frienji> {
        return performMultipartUploadRequest(.PATCH, endpoint: .Profile, headers: createAuthorizationHeaders()) { multipartFormData in
                multipartFormData.appendImage(cover, key: "cover_photo")
                multipartFormData.appendText(name, key: "username")
                multipartFormData.appendText(bio, key: "who_you_are")
                multipartFormData.appendAvatar(avatar)
            }
            .doOnNext {
                self.settings.userData = $0
            }
            .decode()
            .observeOn(MainScheduler.instance)
    }

}

// MARK: - Upload helpers

extension FrienjiApi {

    func performMultipartUploadRequest(
        method: Alamofire.Method,
        endpoint: FrienjiApi.Endpoint,
        headers: [String: String]? = nil,
        multipartFormData: MultipartFormData -> Void
        ) -> Observable<AnyObject> {

        let request: Observable<Request> = Observable.create { observer -> Disposable in
            let manager = Manager.sharedInstance
            manager.upload(method, endpoint.requestUrl,
                headers: headers,
                multipartFormData: multipartFormData,
                encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold,
                encodingCompletion: { result in
                    switch result {
                    case let .Success(request, _, _):
                        observer.onNext(request)
                        request.response { (_, _, _, error) -> Void in
                            if let error = error {
                                observer.on(.Error(error as ErrorType))
                            } else {
                                observer.on(.Completed)
                            }
                        }
                        if !manager.startRequestsImmediately {
                            request.resume()
                        }
                    case let .Failure(error):
                        observer.onError(error)
                    }
                }
            )

            return NopDisposable.instance
        }

        return request
            .flatMap {
                $0
                    .validate(statusCode: 200 ..< 300)
                    .validate(contentType: ["application/json"])
                    .rx_JSON()
            }
            .retry(FrienjiApi.RetryCount)
            .trackActivity(activityIndicator)
            .doOnError { print($0) }
    }

}

// MARK: - Push notification helpers

extension FrienjiApi {

    enum NotificationType: Int {
        case ApplePushNotifications = 0
        case FirebaseCloudMessaging = 1
    }

    func createNotificationParameters() -> [String: AnyObject] {
        guard let hardwareToken = UIDevice.currentDevice().identifierForVendor?.UUIDString, registrationToken = settings.pushNotificationsToken else {
            return [:]
        }

        return [
            "hardware_token": hardwareToken,
            "registration_token": registrationToken,
            "device_type": NotificationType.ApplePushNotifications.rawValue,
        ]
    }

}

// MARK: - Pagination helpers

extension FrienjiApi {

    static let kResultsPerPage = 20

    func createPaginationParameters(page: Int?, resultsPerPage: Int = FrienjiApi.kResultsPerPage) -> [String: AnyObject] {
        guard let page = page else {
            return [:]
        }
        return [
            "page": page,
            "results_per_page": resultsPerPage,
        ]
    }

    private func performRequestRecursively<Element>(loadedSoFar: [Element], nextPage: Int?, loadMoreTrigger: Observable<Void>?, requestHandler: (Int?) -> Observable<(results: [Element], nextPage: Int?)>) -> Observable<[Element]> {
        return requestHandler(nextPage)
            .flatMap { response -> Observable<[Element]> in
                let appended = loadedSoFar + response.results
                guard let nextPage = response.nextPage, trigger = loadMoreTrigger else {
                    return Observable.just(appended)
                }

                return [
                    Observable.just(appended),
                    Observable.never().takeUntil(trigger),
                    self.performRequestRecursively(appended, nextPage: nextPage, loadMoreTrigger: loadMoreTrigger, requestHandler: requestHandler)
                ].concat()
        }
    }

    private func performRequestRecursivelyReversed<Element>(loadedSoFar: [Element], previousPage: Int?, loadMoreTrigger: Observable<Void>?, requestHandler: (Int?) -> Observable<(results: [Element], previousPage: Int?)>) -> Observable<[Element]> {
        return requestHandler(previousPage)
            .flatMap { response -> Observable<[Element]> in
                let appended = response.results + loadedSoFar
                guard let previousPage = response.previousPage, trigger = loadMoreTrigger else {
                    return Observable.just(appended)
                }

                return [
                    Observable.just(appended),
                    Observable.never().takeUntil(trigger),
                    self.performRequestRecursivelyReversed(appended, previousPage: previousPage, loadMoreTrigger: loadMoreTrigger, requestHandler: requestHandler)
                ].concat()
        }
    }

    func getPaginated<Element>(endpoint: FrienjiApi.Endpoint, loadMoreTrigger: Observable<Void>?, decodeHandler: (JSON) -> Decoded<[Element]>) -> Observable<[Element]> {
        return performRequestRecursively([Element](), nextPage: nil, loadMoreTrigger: loadMoreTrigger) { page in
            let request = self.performRequest(.GET,
                    endpoint: endpoint,
                    parameters: self.createPaginationParameters(page),
                    encoding: .URL,
                    headers: self.createAuthorizationHeaders()
                )
                .shareReplay(1)
            let elements: Observable<[Element]> = request.decode(decodeHandler)
            let nextPage: Observable<Int?> = request.decode { $0 <|? ["meta", "next_page"] }

            return Observable.combineLatest(elements, nextPage) { (results: $0, nextPage: $1) }
        }
    }

    func getPaginatedReversed<Element>(endpoint: FrienjiApi.Endpoint, loadMoreTrigger: Observable<Void>?, decodeHandler: (JSON) -> Decoded<[Element]>) -> Observable<[Element]> {
        return performRequestRecursivelyReversed([Element](), previousPage: nil, loadMoreTrigger: loadMoreTrigger) { page in
            let request = self.performRequest(.GET,
                    endpoint: endpoint,
                    parameters: self.createPaginationParameters(page),
                    encoding: .URL,
                    headers: self.createAuthorizationHeaders()
                )
                .shareReplay(1)
            let elements: Observable<[Element]> = request.decode(decodeHandler)
            let previousPage: Observable<Int?> = request.decode { $0 <|? ["meta", "prev_page"] }

            return Observable.combineLatest(elements, previousPage) { (results: $0, previousPage: $1) }
        }
    }

}
