//
//  JSQMessagesInputToolbar.swift
//  Frienji
//
//  Created by Piotr Łyczba on 22/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import JSQMessagesViewController
import RxSwift
import RxCocoa

extension JSQMessagesInputToolbar {

    private static let kImageAttachmentTag = 101
    private static let kImageAttachmentFrame = CGRect(x: 5, y: -35, width: 30, height: 30)

    private static let kLocationAttachmentTag = 102
    private static let kLocationAttachmentFrame = CGRect(x: 40, y: -35, width: 30, height: 30)

    var imageAttachment: UIImageView? {
        set {
            imageAttachment?.removeFromSuperview()
            guard let imageView = newValue else {
                return
            }

            imageView.frame = JSQMessagesInputToolbar.kImageAttachmentFrame
            imageView.tag = JSQMessagesInputToolbar.kImageAttachmentTag
            addSubview(imageView)
        }
        get {
            return subviews
                .filter { $0.tag == JSQMessagesInputToolbar.kImageAttachmentTag }
                .first as? UIImageView
        }
    }

    var locationAttachment: UIImageView? {
        set {
            locationAttachment?.removeFromSuperview()
            guard let locationView = newValue else {
                return
            }

            locationView.frame = JSQMessagesInputToolbar.kLocationAttachmentFrame
            locationView.tag = JSQMessagesInputToolbar.kLocationAttachmentTag
            addSubview(locationView)
        }
        get {
            return subviews
                .filter { $0.tag == JSQMessagesInputToolbar.kLocationAttachmentTag }
                .first as? UIImageView
        }
    }

    var rx_imageAttachment: AnyObserver<UIImage?> {
        return UIBindingObserver(UIElement: self) { inputToolbar, image in
            let imageView = UIImageView()
            imageView.image = image
            inputToolbar.imageAttachment = imageView
            if image != nil {
                self.contentView.rightBarButtonItem.enabled = true
            }
        }
        .asObserver()
    }
    
    var rx_locationAttachment: AnyObserver<UIImage?> {
        return UIBindingObserver(UIElement: self) { inputToolbar, locationImage in
            let locationView = UIImageView()
            locationView.image = locationImage
            inputToolbar.locationAttachment = locationView
            if locationImage != nil {
                self.contentView.rightBarButtonItem.enabled = true
            }
        }
        .asObserver()
    }

}
