//
//  MultipartFormData+CustomData.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Alamofire

private let kCompressionQuality: CGFloat = 0.8

extension MultipartFormData {

    func appendLocation(latitude: Double?, longitude: Double?) {
        let latitudeData = latitude.map(String.init).flatMap { $0.dataWithUTF8Encoding() }
        let longitudeData = longitude.map(String.init).flatMap { $0.dataWithUTF8Encoding() }
        if let latitudeData = latitudeData, longitudeData = longitudeData {
            appendBodyPart(data: latitudeData, name: "location[latitude]")
            appendBodyPart(data: longitudeData, name: "location[longitude]")
        }
    }

    func appendImage(image: UIImage?, key: String) {
        let imageData = image.flatMap { UIImageJPEGRepresentation($0, kCompressionQuality) }
        if let imageData = imageData {
            appendBodyPart(data: imageData, name: key, fileName: "coverImage", mimeType: "image/*")
        }
    }

    func appendText(text: String?, key: String) {
        if let contentData = text?.dataWithUTF8Encoding() {
            appendBodyPart(data: contentData, name: key)
        }
    }

    func appendAvatar(avatar: Avatar?) {
        let nameData = avatar?.name.dataWithUTF8Encoding()
        let typeData = avatar.map { "\($0.type.rawValue)" }.flatMap { $0.dataWithUTF8Encoding() }
        if let nameData = nameData, typeData = typeData {
            appendBodyPart(data: nameData, name: "avatar_attributes[avatar_name]")
            appendBodyPart(data: typeData, name: "avatar_attributes[avatar_type]")
        }
    }

}
