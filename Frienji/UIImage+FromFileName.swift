//
//  UIImage+FromFileName.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import CoreGraphics
import Foundation

extension UIImage {
    static private let kAvatarSize: CGFloat = 100
    
    @nonobjc static let kDefaultAvatar = "av0.png"
    
    class func getAnimatedImageFrom(fileName: String) -> UIImage? {
        let imageName: NSString = fileName as NSString
        
        if let resourcePath = NSBundle.mainBundle().pathForResource(imageName.stringByDeletingPathExtension, ofType: imageName.pathExtension),
            imageData: NSData = NSData(contentsOfFile: resourcePath) {
            return UIImage.sd_animatedGIFWithData(imageData)
        }

        return nil
    }

    class func getEmojiAsImage(emoji: String) -> UIImage {
        let emoji = emoji.emojiUnescaped
        guard emoji.characters.count == 1 else {
            return UIImage(named: UIImage.kDefaultAvatar) ?? UIImage()
        }
        
        let emojiLbl = UILabel(frame: CGRectMake(0, 0, UIImage.kAvatarSize, UIImage.kAvatarSize))
        emojiLbl.font = UIFont(name: "AppleColorEmoji", size: kAvatarSize)
        emojiLbl.text = emoji
        emojiLbl.adjustsFontSizeToFitWidth = true

        UIGraphicsBeginImageContextWithOptions(emojiLbl.bounds.size, false, 0.0)
        emojiLbl.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }

}
