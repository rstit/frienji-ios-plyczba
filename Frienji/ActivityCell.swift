//
//  ActivityCell.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var createdAt: UILabel!

    func configure(activity: Activity<Any>) {
        avatar.image = activity.owner.avatar.avatarImage
        details.text = activity.key.rawValue
        details.text = String.localizedStringWithFormat(NSLocalizedString(activity.key.rawValue, comment: ""), activity.owner.username)
        createdAt.text = activity.createdAt.timeAgoInWords()
    }

}
