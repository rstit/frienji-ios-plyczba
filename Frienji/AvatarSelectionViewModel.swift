//
//  AvatarSelectionViewModel.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AvatarSelectionViewModel
{
    private (set) var entryType: AvatarType
    
    private var emojis:[String]!
    
    private let kImagePrefix = "image"
    private let kImageExtension = "png"
    private let kNumberOfImages = 9
    
    private let kAnimatedImagePrefix = "avatar"
    private let kAnimatedImageExtension = "gif"
    private let kNumberOfAnimatedImages = 10
    
    //MARK: - Private methods
    
    init()
    {
        self.entryType = .Emoji
    }
    
    private func loadEmojis()
    {
        self.emojis = Array(String.emojiDictionary.values)
    }
    
    private func getEmojiAtIndexPath(indexPath:NSIndexPath) -> String
    {
        return self.emojis[indexPath.row]
    }
    
    //like av0.png
    private func getImageAtIndexPath(indexPath:NSIndexPath) -> String
    {
        return "\(self.kImagePrefix)\(indexPath.row+1).\(self.kImageExtension)"
    }
    
    //like aav0.png
    private func getAnimatedImageAtIndexPath(indexPath:NSIndexPath) -> String
    {
        return "\(self.kAnimatedImagePrefix)\(indexPath.row+1).\(self.kAnimatedImageExtension)"
    }
    
    //MARK: - Public methods
    
    func setEntryType(entryType: AvatarType)
    {
        self.entryType = entryType
        if(self.entryType == .Emoji)
        {
            self.loadEmojis()
        }
    }
    
    func getNumberOfItems() -> Int
    {
        switch self.entryType {
        case .Emoji:
            return self.emojis.count
        case .Image:
            return self.kNumberOfImages
        case .AnimatedImage:
            return self.kNumberOfAnimatedImages
        }
    }
    
    func getItemForIndexPath(indexPath:NSIndexPath)->String
    {
        switch self.entryType {
        case .Emoji:
            return self.getEmojiAtIndexPath(indexPath)
        case .Image:
            return self.getImageAtIndexPath(indexPath)
        case .AnimatedImage:
            return self.getAnimatedImageAtIndexPath(indexPath)
        }
    }
    
    func storeAvatarInfo(avatarName:String)
    {
        Settings.sharedInstance.userAvatarName = avatarName.emojiEscaped
        Settings.sharedInstance.userAvatarGroup = self.entryType
    }
}
