//
//  CLLocation+MediaView.swift
//  Frienji
//
//  Created by Piotr Łyczba on 23/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MapKit

private let kMapRegionDistance = 500.0

extension CLLocation {

    func takeMapSnapshot(completion: (UIImage?, ErrorType?) -> Void) {
        let options = MKMapSnapshotOptions()
        options.region = MKCoordinateRegionMakeWithDistance(coordinate, kMapRegionDistance, kMapRegionDistance)
        options.size = UIScreen.mainScreen().bounds.size
        options.scale = UIScreen.mainScreen().scale

        let snapshotter = MKMapSnapshotter(options: options)
        snapshotter.startWithCompletionHandler { snapshot, error in
            guard error == nil else {
                completion(nil, error)
                return
            }
            guard let snapshot = snapshot else {
                completion(nil, nil)
                return
            }

            completion(self.drawSnapshotImage(snapshot), nil)
        }
    }

    var mapSnapshot: Observable<UIImage> {
        return Observable.create { observer in
            self.takeMapSnapshot { image, error in
                if let error = error {
                    observer.onError(error)
                    return
                }
                guard let image = image else {
                    observer.onCompleted()
                    return
                }
                observer.onNext(image)
                observer.onCompleted()
            }

            return NopDisposable.instance
        }
    }

    private func drawSnapshotImage(snapshot: MKMapSnapshot) -> UIImage? {
        let image = snapshot.image
        let pin = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
        var coordinatePoint = snapshot.pointForCoordinate(self.coordinate)
        coordinatePoint.x += pin.centerOffset.x - (CGRectGetWidth(pin.bounds) / 2.0)
        coordinatePoint.y += pin.centerOffset.y - (CGRectGetHeight(pin.bounds) / 2.0)

        UIGraphicsBeginImageContextWithOptions(image.size, true, image.scale)
        image.drawAtPoint(CGPointMake(0, 0))
        pin.image?.drawAtPoint(coordinatePoint)
        let finalImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return finalImage
    }

}
