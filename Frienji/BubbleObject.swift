//
//  BubbleObject.swift
//  Traces
//
//  Created by Adam Szeremeta on 14.07.2016.
//  Copyright Â© 2016 Ripple Inc. All rights reserved.
//

import Foundation
import OpenGLES
import AVFoundation
import RxSwift
import RxCocoa
import SDWebImage

class BubbleObject: Hashable, Equatable {

    static let kCoordinateSpaceSize:CGFloat = 2 //from -1 to 1
    static let kLowPassFilterHorizontalValue:CGFloat = 0.05
    static let kLowPassFilterVerticalValue:CGFloat = 0.2
    static let kSizeAnimationDelta:Double = 0.03
    static let kPositionAnimationDelta:Double = 0.01

    private (set) var bubbleSize: CGSize = CGSizeZero
    private (set) var bubblePosition: CGPoint = CGPointZero
    private (set) var bubbleBearing: CGFloat?

    private var logoTextureData:[GLubyte]?
    static var logoTextureWidth:Int32 = Int32(ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale)
    static var logoTextureHeight:Int32 = Int32(ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale)
    static var logoTexturePointer:GLuint?

    private var xFloatingDirection:CGFloat = 1
    private var yFloatingDirection:CGFloat = 1
    private var xFloatingValue:CGFloat = 0
    private var yFloatingValue:CGFloat = 0

    private var currentSizeAnimationDelta:Double = 0
    private var sizeDisposeBag:DisposeBag? = nil

    private var currentLocationAnimationDelta:Double = 0
    private var locationAnimationDisposeBag:DisposeBag? = nil

    private var disposeBag = DisposeBag()

    // MARK: Bubble texture data

    private var bubbleVertices: (Vertex, Vertex, Vertex, Vertex) = (
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 1)),
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 1))
    )

    private var bubbleIndices: (GLubyte, GLubyte, GLubyte, GLubyte, GLubyte, GLubyte) = (
        0, 1, 2,
        2, 3, 0
    )

    // MARK: Properties

    private (set) var shouldBubbleBeDrawn = false

    private var indexBuffer: GLuint = GLuint()
    private var vertexBuffer: GLuint = GLuint()

    private (set) var trace:Frienji!

    // MARK: Hashable

    var hashValue : Int {
        get {
            return "\(self.trace.dbID)".hashValue
        }
    }

    // MARK: Life cycle

    init(trace:Frienji) {
        self.trace = trace

        loadTraceLogoAsTexture()
        setupVBOs()

        //random for start floating direction
        self.xFloatingDirection = Int(arc4random_uniform(UInt32(2))) == 0 ? -1 : 1
        self.yFloatingDirection = Int(arc4random_uniform(UInt32(2))) == 0 ? -1 : 1

        Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in

            self?.addRandomFactorToBubblePosition()

            }).addDisposableTo(self.disposeBag)
    }

    // MARK: Setup

    private func setupVBOs() {
        // Setup VertexColor Buffer Objects

        glGenBuffers(1, &self.vertexBuffer)
        glGenBuffers(1, &self.indexBuffer)
    }

    // MARK: Texture

    private func loadTraceLogoAsTexture() {
        //scale image
        let image = trace.avatar.avatarImage
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {

            let scaledImage = self.scaleTraceLogoImage(image)
            self.createLogoTextureDataFromImage(scaledImage)
        }
    }

    private func scaleTraceLogoImage(image:UIImage) -> UIImage {
        let maxWidth = ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width
        return image.scaleToWidth(maxWidth)
    }

    private func createLogoTextureDataFromImage(logoImage:UIImage) {

        guard let logoCGImage = logoImage.CGImage else {

            return
        }

        let logoWidth = CGImageGetWidth(logoCGImage)
        let logoHeight = CGImageGetHeight(logoCGImage)

        var spriteData: [GLubyte] = Array(count: Int(logoWidth * logoHeight * 4), repeatedValue: 0)
        let bitmapInfo = CGImageAlphaInfo.PremultipliedLast.rawValue

        if let colorSpace = CGImageGetColorSpace(logoCGImage), spriteContext = CGBitmapContextCreate(&spriteData, logoWidth, logoHeight, 8, logoWidth * 4, colorSpace, bitmapInfo) {
            CGContextDrawImage(spriteContext, CGRectMake(0, 0, CGFloat(logoWidth) , CGFloat(logoHeight)), logoCGImage)
            self.logoTextureData = spriteData
        }
    }

    class func activateTextureUnit(textureLogoUniform:GLuint) {
        glActiveTexture(UInt32(GL_TEXTURE1))

        var pointer = GLuint()
        glGenTextures(1, &pointer)
        glBindTexture(UInt32(GL_TEXTURE_2D), pointer)
        BubbleObject.logoTexturePointer = pointer

        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MAG_FILTER), GL_NEAREST)
        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MIN_FILTER), GL_NEAREST)

        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_S), Int32(GL_CLAMP_TO_EDGE))
        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_T), Int32(GL_CLAMP_TO_EDGE))

        createLogoTexture(textureLogoUniform)
    }

    class func createLogoTexture(textureLogoUniform:GLuint) {
        //allocate texture data
        glTexImage2D(UInt32(GL_TEXTURE_2D), 0, GL_RGBA, BubbleObject.logoTextureWidth, BubbleObject.logoTextureHeight, 0, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), nil)
        glUniform1i(Int32(textureLogoUniform), 1)
    }

    private func replaceLogoTextureContent() {
        //check if we have logo texture
        if let textureData = self.logoTextureData {
            glTexSubImage2D(UInt32(GL_TEXTURE_2D), 0, 0, 0, BubbleObject.logoTextureWidth, BubbleObject.logoTextureHeight, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), textureData)
        }
    }

    class func destroyLogoTexture() {
        if var pointer = BubbleObject.logoTexturePointer {
            glDeleteTextures(1, &pointer)
        }
    }

    // MARK: Animations

    private func animateBubbleSize(newSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat) {
        //calculate size
        let newWidth = newSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
        let newHeight = newSize * BubbleObject.kCoordinateSpaceSize / viewportHeight

        //do smooth transition between old and new size
        self.currentSizeAnimationDelta = 0
        self.sizeDisposeBag = DisposeBag()

        Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in

            self?.currentSizeAnimationDelta += BubbleObject.kSizeAnimationDelta

            if let strongSelf = self where strongSelf.currentSizeAnimationDelta < 1 {

                let finalWidth = strongSelf.bubbleSize.width + CGFloat(BubbleObject.kSizeAnimationDelta) * (newWidth - strongSelf.bubbleSize.width)
                let finalHeight = strongSelf.bubbleSize.height + CGFloat(BubbleObject.kSizeAnimationDelta) * (newHeight - strongSelf.bubbleSize.height)

                self?.bubbleSize = CGSizeMake(finalWidth, finalHeight)
                self?.updateBubbleVertices()

            } else {

                self?.sizeDisposeBag = nil
            }

            }).addDisposableTo(self.sizeDisposeBag!)
    }

    // MARK: Data

    func getBoundingBoxForScreenPixelCoordinates(screenSize:CGSize, viewportSize:CGSize) -> CGRect {
        let ratio = screenSize.width / viewportSize.width
        let heightDiff = abs(screenSize.height - viewportSize.height) / 2

        let width = screenSize.width * self.bubbleSize.width * ratio
        let height = screenSize.height * self.bubbleSize.height * ratio

        let originX = screenSize.width * (1 + self.bubblePosition.x) / BubbleObject.kCoordinateSpaceSize
        let originY = screenSize.height - ((screenSize.height * (1 + self.bubblePosition.y) / BubbleObject.kCoordinateSpaceSize) + height) - heightDiff/2

        return CGRectMake(originX, originY, width, height)
    }

    func calculateBubbleSizeForDesiredSize(desiredSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat, animated:Bool) {
        precondition(viewportWidth > 0)
        precondition(viewportHeight > 0)

        if animated {

            self.animateBubbleSize(desiredSize, viewportWidth: viewportWidth, viewportHeight: viewportHeight)

        } else {

            let newWidth = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
            let newHeight = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportHeight

            self.bubbleSize = CGSizeMake(newWidth, newHeight)
            self.updateBubbleVertices()
        }
    }

    func calculateBubblePositionForYMotion(inclination:CGFloat) {
        let kDefaultBubbleYOrigin = -self.bubbleSize.height/2
        let newYPosition = kDefaultBubbleYOrigin - 2 * inclination

        //low pass filter
        let yPosition = BubbleObject.kLowPassFilterVerticalValue * self.bubblePosition.y + (1.0 - BubbleObject.kLowPassFilterVerticalValue) * newYPosition
        self.bubblePosition = CGPointMake(self.bubblePosition.x, yPosition)

        updateBubbleVertices()
    }

    func calculateBubblePositionForBearing(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat, animated:Bool) {
        precondition(minFieldOfViewAngle > 0)
        precondition(maxFieldOfViewAngle > 0)

        if animated {
            //do smooth transition between old and new location
            self.currentLocationAnimationDelta = 0
            self.locationAnimationDisposeBag = DisposeBag()

            //check animation direction
            let current = self.bubbleBearing ?? 0
            let rightDistance = current <= bearing ? bearing - current : 360 - current + bearing
            let leftDistance = current < bearing ? current + 360 - bearing : current - bearing
            let distance = rightDistance <= leftDistance ? rightDistance : -leftDistance

            Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in
                self?.currentLocationAnimationDelta += BubbleObject.kPositionAnimationDelta

                if let strongSelf = self, let currentBearing = self?.bubbleBearing where strongSelf.currentLocationAnimationDelta <= 1 {
                    let finalBearing = currentBearing + CGFloat(BubbleObject.kPositionAnimationDelta) * distance
                    strongSelf.bubbleBearing = finalBearing > 0 ? finalBearing % 360 : 360 + finalBearing

                } else {
                    self?.locationAnimationDisposeBag = nil
                    self?.bubbleBearing = bearing
                }

                }).addDisposableTo(self.locationAnimationDisposeBag!)

        } else {

            if self.bubbleBearing == nil {
                self.bubbleBearing = bearing
            }

            self.normalizeAndCalculateBubblePositionForAngle(self.bubbleBearing, minFieldOfViewAngle: minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle)
        }
    }

    private func normalizeAndCalculateBubblePositionForAngle(bearing:CGFloat?, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) {
        var minAngle = minFieldOfViewAngle
        var maxAngle = maxFieldOfViewAngle
        var bubbleBearing = bearing ?? 0

        if minFieldOfViewAngle > maxFieldOfViewAngle && bearing > maxFieldOfViewAngle {
            //for example min: 340, bearing 350, max: 40
            maxAngle = maxFieldOfViewAngle + 360

        } else if minFieldOfViewAngle > maxFieldOfViewAngle && bearing < maxFieldOfViewAngle {
            //for example min: 340, bearing 15, max: 40
            maxAngle = maxFieldOfViewAngle + 360
            bubbleBearing = bubbleBearing + 360

        } else if bearing > 360 - (maxFieldOfViewAngle - minFieldOfViewAngle) && !isFieldOfViewOver180Degrees(minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle) {
            //for example min: 10, bearing 350, max: 60
            let distance = abs(360 - bubbleBearing)

            minAngle = minFieldOfViewAngle + distance
            maxAngle = maxFieldOfViewAngle + distance
            bubbleBearing = distance

        } else if bearing < (maxFieldOfViewAngle - minFieldOfViewAngle) && isFieldOfViewOver180Degrees(minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle)  {
            //for example min: 320, bearing 10, max: 350
            let distance = bubbleBearing

            minAngle = minFieldOfViewAngle - distance
            maxAngle = maxFieldOfViewAngle - distance
            bubbleBearing = 360 - distance
        }

        //check if in range
        if self.isBearingInRangeOfFieldOfView(bubbleBearing, minFieldOfViewAngle: minAngle, maxFieldOfViewAngle: maxAngle) {
            updateBubbleHorizontalPosition(bubbleBearing, minFieldOfViewAngle: minAngle, maxFieldOfViewAngle: maxAngle)

        } else {
            self.shouldBubbleBeDrawn = false
        }
    }

    private func isFieldOfViewOver180Degrees(minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) -> Bool {
        return minFieldOfViewAngle > 180 && maxFieldOfViewAngle > 180
    }

    private func isBearingInRangeOfFieldOfView(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) -> Bool {
        let fieldOfViewMargin = (maxFieldOfViewAngle - minFieldOfViewAngle) / 5

        var isBearingInRange = minFieldOfViewAngle < maxFieldOfViewAngle
        isBearingInRange = isBearingInRange && (bearing < maxFieldOfViewAngle + fieldOfViewMargin && bearing > minFieldOfViewAngle - fieldOfViewMargin)

        return isBearingInRange
    }

    private func updateBubbleHorizontalPosition(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) {
        let angleRange = maxFieldOfViewAngle - minFieldOfViewAngle
        let bubbleBearing = angleRange - (maxFieldOfViewAngle - bearing)

        let position = -1 + (bubbleBearing * BubbleObject.kCoordinateSpaceSize / angleRange) //this is center so we have to substract half of the width
        let newPosition = CGPointMake(position - self.bubbleSize.width/2, 0)

        //low pass filter
        let xPosition = BubbleObject.kLowPassFilterHorizontalValue * self.bubblePosition.x + (1.0 - BubbleObject.kLowPassFilterHorizontalValue) * newPosition.x
        self.bubblePosition = CGPointMake(xPosition, self.bubblePosition.y)

        updateBubbleVertices()

        self.shouldBubbleBeDrawn = true
    }

    private func updateBubbleVertices() {
        let position = CGPointMake(self.bubblePosition.x + self.xFloatingValue, self.bubblePosition.y + self.yFloatingValue)

        self.bubbleVertices.0.position = (CFloat(position.x + self.bubbleSize.width), CFloat(position.y), 0) //right bottom
        self.bubbleVertices.1.position = (CFloat(position.x + self.bubbleSize.width), CFloat(position.y + self.bubbleSize.height), 0) //right top
        self.bubbleVertices.2.position = (CFloat(position.x), CFloat(position.y + self.bubbleSize.height), 0) //left top
        self.bubbleVertices.3.position = (CFloat(position.x), CFloat(position.y), 0) //left bottom
    }

    private func addRandomFactorToBubblePosition() {
        let kXMaxRandomFactor:CGFloat = 0.25
        let kYMaxRandomFactor:CGFloat = 0.12

        //switch direction

        if abs(self.xFloatingValue) >= kXMaxRandomFactor {

            self.xFloatingDirection *= -1

        }

        if abs(self.yFloatingValue) >= kYMaxRandomFactor {

            self.yFloatingDirection *= -1
        }

        //add random value

        let xRandom:CGFloat = 0.0035
        let yRandom:CGFloat = 0.0008

        self.xFloatingValue += xRandom * self.xFloatingDirection
        self.yFloatingValue += yRandom * self.yFloatingDirection
    }

    // MARK: Render

    func render(positionSlot:GLuint, textureCoordinateSlot:GLuint, textureUniform:GLuint) {
        if self.shouldBubbleBeDrawn {

            //bind buffers
            glBindBuffer(UInt32(GL_ARRAY_BUFFER), self.vertexBuffer)
            glBufferData(UInt32(GL_ARRAY_BUFFER), Int(sizeofValue(self.bubbleVertices)), &self.bubbleVertices, UInt32(GL_STATIC_DRAW))

            glBindBuffer(UInt32(GL_ELEMENT_ARRAY_BUFFER), self.indexBuffer)
            glBufferData(UInt32(GL_ELEMENT_ARRAY_BUFFER), Int(sizeofValue(self.bubbleIndices)), &self.bubbleIndices, UInt32(GL_STATIC_DRAW))

            //bind logo texture
            self.replaceLogoTextureContent()

            //set up pointers
            let positionSlotPointer = UnsafePointer<Int>(bitPattern: 0)
            glVertexAttribPointer(positionSlot, 3, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), positionSlotPointer)

            let textCoordinatePointer = UnsafePointer<Int>(bitPattern: sizeof(Float) * 3)
            glVertexAttribPointer(textureCoordinateSlot, 2, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), textCoordinatePointer)

            let vertextBufferOffset = UnsafePointer<Int>(bitPattern: 0)
            glDrawElements(UInt32(GL_TRIANGLES), Int32(GLfloat(sizeofValue(self.bubbleIndices)) / GLfloat(sizeofValue(self.bubbleIndices.0))), UInt32(GL_UNSIGNED_BYTE), vertextBufferOffset)
        }
    }

}

// MARK: Equatable

func ==(lhs: BubbleObject, rhs: BubbleObject) -> Bool {

    return lhs.hashValue == rhs.hashValue
}
