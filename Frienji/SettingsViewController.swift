//
//  SettingsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 27/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

// MARK: - Input

struct SettingsInput: Input {

    private var viewModel: SettingsViewModel? = nil

    var avatarSelected: AnyObserver<Avatar?>? {
        return viewModel?.avatarSelected.asObserver()
    }

}

class SettingsViewController: UITableViewController, HasInput, HasDisposeBag {

    var input = SettingsInput()

    // MARK: - Outlets

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var bio: UITextView!
    @IBOutlet weak var cover: UIButton!
    @IBOutlet weak var avatar: UIButton!
    @IBOutlet weak var save: UIBarButtonItem!
    @IBOutlet weak var nameValidationMessage: UILabel!
    @IBOutlet weak var bioValidationMessage: UILabel!

    // MARK: - View model

    var viewModel: SettingsViewModel! {
        didSet {
            input.viewModel = viewModel
            createBindings().forEach(disposeBag.addDisposable)

            viewModel.error
                .showAlertOnApiError(navigationController ?? self)
                .subscribe()
                .addDisposableTo(disposeBag)
        }
    }

    var disposeBag = DisposeBag()

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationHandler = NavigationHandler(sourceViewController: self)
        let attachmentHandler = AttachmentHandler(targetViewController: self, cameraEnabled: true, galleryEnabled: true, locationEnabled: false)
        viewModel = SettingsViewModel(navigationHandler: navigationHandler, attachmentHandler: attachmentHandler)
        viewModel.initialized.onNext()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Settings) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Settings) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Settings)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    // MARK: - Bindings

    func createBindings() -> [Disposable] {
        return [
            name <-> viewModel.name,
            bio <-> viewModel.bio,
            viewModel.avatar --> avatar.rx_image,
            viewModel.cover --> cover.rx_image,
            avatar.rx_tap --> viewModel.avatarTapped,
            save.rx_tap --> viewModel.saved,
            viewModel.nameIsValid --> nameValidationMessage.rx_hidden,
            viewModel.bioIsValid --> bioValidationMessage.rx_hidden,
            viewModel.isValid --> save.rx_enabled,
            tableView.rx_cellSelected(R.reuseIdentifier.logoutCell.identifier).map { _ in } --> viewModel.logoutSelected,
            tableView.rx_cellSelected(R.reuseIdentifier.termsAndConditionCell.identifier).map { _ in } --> viewModel.termsAndConditionSelected,
            tableView.rx_cellSelected(R.reuseIdentifier.contactUsCell.identifier).map { _ in } --> viewModel.contactUsSelected,
        ]
    }

    // MARK: - Table view delegate

    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .None
    }

}

// MARK: - Has attachment

extension SettingsViewController: HasAttachment {

    var attachmentRequested: Observable<Void> {
        return cover.rx_tap.asObservable()
    }

}
