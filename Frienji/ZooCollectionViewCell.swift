//
//  ZooCollectionViewCell.swift
//  Frienji
//
//  Created by bolek on 17.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

protocol ZooCollectionViewCellDelegate:class
{
    func frienjiToDelete(frienji:Frienji)
}

class ZooCollectionViewCell: UICollectionViewCell
{
    static let identifier = "ZooCollectionViewCellIdentifier"
    
    @IBOutlet weak var cover : UIImageView!
    @IBOutlet weak var avatar : UIImageView!
    @IBOutlet weak var userName : UILabel!
    @IBOutlet weak var deleteButton : UIButton!
    
    var frienji: Frienji!
    
    //MARK: - Public methods
    
    func confireCellForFrienji(frienji: Frienji, isViewInEditionMode editing: Bool)
    {
        self.frienji = frienji
        userName.text = frienji.username
        avatar.image = frienji.avatar.avatarImage
        if let backgroundImageUrl = frienji.backgroundImageUrl {
            cover.sd_setImageWithURL(NSURL(string: backgroundImageUrl))
        }
        
        deleteButton.hidden = !editing
    }
    
    //MARK: - Overriden methods
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let pointForDeleteButton = deleteButton.convertPoint(point, fromView: self)
        if CGRectContainsPoint(deleteButton.bounds, pointForDeleteButton) {
            return deleteButton.hitTest(pointForDeleteButton, withEvent: event)
        }
        
        return super.hitTest(point, withEvent: event)
    }
   
}
