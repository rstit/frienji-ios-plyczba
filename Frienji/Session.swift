//
//  Session.swift
//  Frienji
//
//  Created by Piotr Łyczba on 14/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Session {

    let token: String
    let client: String
    let uid: String

}

// MARK: - Decode

extension Session: Decodable {

    static func decode(json: JSON) -> Decoded<Session> {
        return curry(Session.init)
            <^> json <| "token"
            <*> json <| "client"
            <*> json <| "uid"
    }

}
