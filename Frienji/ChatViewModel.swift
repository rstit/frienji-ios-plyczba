//
//  ChatViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 21/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

struct ChatViewModel {

    let messages: [Message]
    let sender: Frienji
    let receiver: Frienji

    init(sender: Frienji, receiver: Frienji, messages: [Message] = []) {
        self.sender = sender
        self.receiver = receiver
        self.messages = messages
    }

    func executeCommand(command: ChatCommand) -> ChatViewModel {
        switch command {
        case let .Load(messages):
            return ChatViewModel(sender: sender, receiver: receiver, messages: messages)
        case let .Send(message):
            return ChatViewModel(sender: sender, receiver: receiver, messages: messages.arrayByAppending(message))
        case let .Receive(message):
            return ChatViewModel(sender: sender, receiver: receiver, messages: messages.arrayByAppending(message))
        }
    }

}

enum ChatCommand {
    case Load(messages: [Message])
    case Send(message: Message)
    case Receive(message: Message)
}
