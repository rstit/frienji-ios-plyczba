//
//  WallPostsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 05/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

class WallPostsViewController: WallViewController {

    // MARK: - Outlets

    @IBOutlet weak var profile: ProfileView!

    override var viewModel: Observable<WallViewModel> {
        return WallViewModel.create(
            withProfileLoaded: input.loaded,
            textSent: textSent,
            imageAttached: attachmentHandler.image,
            locationAttached: attachmentHandler.location,
            loadMoreTriggered: tableView.loadMoreTrigger
        )
    }

    override func createBindings() {
        super.createBindings()

        input.loaded.bindTo(profile.profileLoaded).addDisposableTo(disposeBag)

        postCommented
            .subscribeNext { [unowned self] post in
                self.performSegueWithIdentifier(R.segue.wallPostsViewController.showComments.identifier, sender: Observable.just(post))
            }
            .addDisposableTo(disposeBag)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let wallCommentsViewController = segue.destinationViewController as? WallCommentsViewController, post = sender as? Observable<Post> {
            post.bindTo(wallCommentsViewController.postLoaded).addDisposableTo(disposeBag)
            input.loaded.bindTo(wallCommentsViewController.input.loaded).addDisposableTo(disposeBag)
            wallCommentsViewController.view.backgroundColor = view.backgroundColor
        }
    }

}
