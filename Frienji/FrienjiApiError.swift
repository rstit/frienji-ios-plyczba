//
//  TracesApiError.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CFNetwork
import Alamofire
import RxSwift

class FrienjiApiError {

    enum ErrorCodes: Int {

        case Unauthorized = 401

        var message: String {
            switch self {
            case .Unauthorized:
                return Localizations.api_error.unauthorized
            }
        }

    }

    // MARK: Properties

    private (set) var statusCode: Int?
    private (set) var errorCode: Int?

    var message: String {
        var result = Localizations.api_error.other
        guard let errorCode = self.errorCode else {
            return result
        }

        guard let alamofireErrorCode = Error.Code(rawValue: errorCode) else {
            if errorCode == NSURLErrorNotConnectedToInternet {
                result = Localizations.api_error.no_internet_connection
            }
            return result
        }

        switch alamofireErrorCode {
        case .StatusCodeValidationFailed:
            if let statusCode = self.statusCode {
                result = ErrorCodes(rawValue: statusCode)?.message ?? Localizations.api_error.generic_message + "\(statusCode)"
            }
        default:
            result = Localizations.api_error.other
        }

        return result
    }

    // MARK: Init

    init(statusCode: Int?, errorCode: Int?) {
        self.statusCode = statusCode
        self.errorCode = errorCode
    }

    class func fromResponse(response: Response<AnyObject, NSError>, defaultMessage: String?) -> FrienjiApiError {

        var statusCode: Int? = nil
        var errorCode: Int? = nil

        switch response.result {

        case .Failure(let error):

            statusCode = response.response?.statusCode
            errorCode = error.code

        default:
            break
        }

        return FrienjiApiError(statusCode: statusCode, errorCode: errorCode)
    }

    class func fromError(error: ErrorType) -> FrienjiApiError {
        let error = error as NSError
        return FrienjiApiError(statusCode: error.userInfo[Error.UserInfoKeys.StatusCode] as? Int, errorCode: error.code)
    }

}
