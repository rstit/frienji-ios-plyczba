//
//  WallCellModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import TimeAgoInWords
import RxSwift
import CoreLocation

enum PostCommand {
    case Load(post: Post)
    case Like
    case Unlike
}

struct PostViewModel {

    let authorName: String
    let createdAt: String
    let details: String
    let avatar: UIImage?
    let likeText: String
    let imageUrl: NSURL?
    let location: CLLocation?

    init(authorName: String = "", createdAt: String = "", details: String = "", avatar: UIImage? = nil, likeText: String = Localizations.wall.like, imageUrl: NSURL? = nil, location: CLLocation? = nil) {
        self.authorName = authorName
        self.createdAt = createdAt
        self.details = details
        self.avatar = avatar
        self.likeText = likeText
        self.imageUrl = imageUrl
        self.location = location
    }

    func executeCommand(command: PostCommand) -> PostViewModel {
        switch command {
        case let .Load(post):
            return PostViewModel(
                authorName: post.author?.username ?? "",
                createdAt: post.createdAt.timeAgoInWords() ?? "",
                details: post.message ?? "",
                avatar: post.author?.avatar.avatarImage,
                likeText: likeText(post.liked),
                imageUrl: post.attachmentUrl.flatMap(NSURL.init),
                location: locationFromLatitude(post.latitude, longitude: post.longitude)
            )
        case .Like:
            return PostViewModel(
                authorName: authorName,
                createdAt: createdAt,
                details: details,
                avatar: avatar,
                likeText: likeText(true),
                imageUrl: imageUrl,
                location: location
            )
        case .Unlike:
            return PostViewModel(
                authorName: authorName,
                createdAt: createdAt,
                details: details,
                avatar: avatar,
                likeText: likeText(false),
                imageUrl: imageUrl,
                location: location
            )
        }
    }

    private func likeText(liked: Bool) -> String {
        return liked ? Localizations.wall.unlike : Localizations.wall.like
    }

    private func locationFromLatitude(latitude: Double?, longitude: Double?) -> CLLocation? {
        guard let latitude = latitude, longitude = longitude else {
            return nil
        }

        return CLLocation(latitude: latitude, longitude: longitude)
    }

}

extension PostViewModel {

    static func viewModel(postLoaded post: Observable<Post>, liked like: Observable<Void>, unliked unlike: Observable<Void>) -> Observable<PostViewModel> {
        let loadCommand = post.map(PostCommand.Load)
        let likeCommand = like
            .withLatestFrom(post)
            .flatMap(FrienjiApi.sharedInstance.likePost)
            .map { PostCommand.Like }
        let unlikeCommand = unlike
            .withLatestFrom(post)
            .flatMap(FrienjiApi.sharedInstance.likePost)
            .map { PostCommand.Unlike }

        return Observable.of(loadCommand, likeCommand, unlikeCommand).merge()
            .scan(PostViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)
    }

}
