//
//  UIColor+Traces.swift
//  Traces
//
//  Created by Adam Szeremeta on 22.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

enum AppColors: String {
    case AppMapBlueBlueColor = "appMapBlueBlueColor"
    case AppPurpleColor = "appPurpleColor"
    case AppLightGreenColor = "appLightGreenColor"
    case AppLightGreenSelectedCellColor = "appLightGreenSelectedCellColor"
    case AppOrangePlaceholderColor = "appOrangePlaceholderColor"
    case AppLightBlueColor = "appLightBlueColor"
    case AppPaleRoseColor = "appPaleRoseColor"
    case AppLightGrayColor = "appLightGrayColor"
    case AppLightGraySelectedColor = "appLightGraySelectedColor"
    case AppGrayColor = "appGrayColor"
    case AppMessageLightGreenColor = "appMessageLightGreenColor"
    case AppMessageLightEcruColor = "appMessageLightEcruColor"
    case AppCyanColor = "appCyanColor"
    case AppLightCyanColor = "appLightCyanColor"
}

extension UIColor {
    @nonobjc static private let cache = NSCache()

    class func appMapBlueBlueColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppMapBlueBlueColor.rawValue) as? UIColor {
            return cachedVersion
        }
        else {
            let color = UIColor(red: 43.0 / 255.0, green: 165.0 / 255.0, blue: 175.0 / 255.0, alpha: 1.0)
            UIColor.cache.setObject(color, forKey: AppColors.AppMapBlueBlueColor.rawValue)
            return color
        }
    }

    class func appPurpleColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppPurpleColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 104/255.0, green: 24/255.0, blue: 151/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppPurpleColor.rawValue)
            return color
        }
    }

    class func appLightGreenColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightGreenColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 176/255.0, green: 254/255.0, blue: 118/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightGreenColor.rawValue)
            return color
        }
    }

    class func appLightGreenSelectedCellColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightGreenSelectedCellColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 186/255.0, green: 241/255.0, blue: 155/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightGreenSelectedCellColor.rawValue)
            return color
        }
    }

    class func appOrangePlaceholderColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppOrangePlaceholderColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 255/255.0, green: 120/255.0, blue: 75/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppOrangePlaceholderColor.rawValue)
            return color
        }
    }

    class func appLightBlueColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightBlueColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 0/255.0, green: 216/255.0, blue: 198/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightBlueColor.rawValue)
            return color
        }
    }

    class func appPaleRoseColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppPaleRoseColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 234/255.0, green: 82/255.0, blue: 111/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppPaleRoseColor.rawValue)
            return color
        }
    }

    class func appLightGrayColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightGrayColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightGrayColor.rawValue)
            return color
        }
    }

    class func appLightGraySelectedColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightGraySelectedColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 210/255.0, green: 210/255.0, blue: 210/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightGraySelectedColor.rawValue)
            return color
        }
    }

    class func appGrayColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppGrayColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(colorLiteralRed: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 1)
            UIColor.cache.setObject(color, forKey: AppColors.AppGrayColor.rawValue)
            return color
        }
    }

    class func appCyanColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppCyanColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(red: 48/255.0, green: 195/255.0, blue: 173/255.0, alpha: 1.0)
            UIColor.cache.setObject(color, forKey: AppColors.AppCyanColor.rawValue)
            return color
        }
    }

    class func appLightCyanColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppLightCyanColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(red: 108/255.0, green: 243/255.0, blue: 180/255.0, alpha: 1.0)
            UIColor.cache.setObject(color, forKey: AppColors.AppLightCyanColor.rawValue)
            return color
        }
    }

    class func appMessageLightGreenColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppMessageLightGreenColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(red: 197/255.0, green: 255/255.0, blue: 23/255.0, alpha: 1.0)
            UIColor.cache.setObject(color, forKey: AppColors.AppMessageLightGreenColor.rawValue)
            return color
        }
    }

    class func appMessageLightEcruColor() -> UIColor {
        if let cachedVersion = cache.objectForKey(AppColors.AppMessageLightEcruColor.rawValue) as? UIColor {
            return cachedVersion
        } else {
            let color = UIColor(red: 252/255.0, green: 255/255.0, blue: 244/255.0, alpha: 1.0)
            UIColor.cache.setObject(color, forKey: AppColors.AppMessageLightEcruColor.rawValue)
            return color
        }
    }

}
