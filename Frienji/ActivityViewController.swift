//
//  ActivityViewController.swift
//  Frienji
//
//  Created by bolek on 04.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController, StoryboardLoad
{
    static var storyboardId: String = "ActivityView"
    static var storyboardControllerId = "ActivityViewController"
    
    @IBOutlet var spinerLbl: UILabel!
    
    private let kAnimationPathSide:CGFloat = 200
    
    //MARK: - Private functions
    
    private func setupSpinner()
    {
        let textAnimation = CAKeyframeAnimation(keyPath: "position")
        textAnimation.duration = 1.5
        textAnimation.path = UIBezierPath.init(ovalInRect: CGRectMake((self.view.frame.size.width - self.kAnimationPathSide) / 2, (self.view.frame.size.height - self.kAnimationPathSide) / 2, self.kAnimationPathSide, self.kAnimationPathSide)).CGPath
        textAnimation.rotationMode = kCAAnimationRotateAutoReverse
        textAnimation.calculationMode = kCAAnimationCubicPaced
        textAnimation.removedOnCompletion = false
        textAnimation.repeatCount = FLT_MAX
        self.spinerLbl.layer.addAnimation(textAnimation, forKey: "position")
    }
    
    //MARK: - Public functions
    
    func setupForFullScreen()
    {
        self.view.frame = (UIApplication.sharedApplication().keyWindow?.bounds)!
        self.setupSpinner()
    }
    
    func setupForFrame(frame:CGRect)
    {
        self.view.frame = frame
        self.setupSpinner()
    }
    
    func hide()
    {
        self.view.removeFromSuperview()
        self.spinerLbl.layer.removeAllAnimations()
    }
    
    //MARK: - Class functions
    
    class func getActivityInFullScreen(emoji:String, inView view:UIView) -> ActivityViewController
    {
        let viewCont = ActivityViewController.loadFromStoryboard()
        viewCont.setupForFullScreen()
        view.addSubview(viewCont.view)
        viewCont.spinerLbl.text = emoji
        return viewCont
    }
    
    class func getActivityInFrame(frame:CGRect, emoji:String, inView view:UIView) -> ActivityViewController
    {
        let viewCont = ActivityViewController.loadFromStoryboard()
        viewCont.spinerLbl.text = emoji
        viewCont.setupForFrame(frame)
        
        view.addSubview(viewCont.view)
        
        return viewCont
    }
}
