//
//  ArPointersView.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class ArPointersView : UIView {
    
    private let kDegreesToRadians = M_PI / 180.0
    private let kRadiansToDegrees = 180.0 / M_PI
    private let kPointerImage = UIImage(named: "ar_arrow")
    
    private var pointers = [Int64: UIImageView]()
    
    // MARK: Data
    
    func updatePointersForBubbleObjects(bubbleObjects:[BubbleObject], userHeading:Double?, cameraAngle:CGFloat) {

        guard let heading = userHeading else {
            
            return
        }
        
        //get pointers ids
        let ids = bubbleObjects.map { (bubble:BubbleObject) -> Int64 in
            
            return bubble.trace.dbID
        }
        
        //remove old pointers
        for (pointerId, pointer) in self.pointers {
            
            if !ids.contains(pointerId) {
                
                pointer.removeFromSuperview()
                self.pointers[pointerId] = nil
            }
        }
        
        //add new pointers
        for bubbleObject in bubbleObjects {
            
            if self.pointers[bubbleObject.trace.dbID] == nil {
                
                self.addPointerForBubbleObject(bubbleObject)
            }
            
            //update pointer
            self.updatePointerPositionForBubbleObject(bubbleObject, userHeading: CGFloat(heading), cameraAngle: cameraAngle)
        }
    }
    
    // MARK: Pointers
    
    private func addPointerForBubbleObject(bubbleObject:BubbleObject) {
        //create pointer view and add it in the center
        
        let imageView = UIImageView(image: self.kPointerImage)
        imageView.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
        
        self.addSubview(imageView)
        
        self.pointers[bubbleObject.trace.dbID] = imageView
    }
    
    private func updatePointerPositionForBubbleObject(bubbleObject:BubbleObject, userHeading:CGFloat, cameraAngle:CGFloat) {
        if let imageView = self.pointers[bubbleObject.trace.dbID] {
            
            let bubbleBearing = bubbleObject.bubbleBearing ?? 0
            
            //check if trace is closer to left or right
            let rightDistance = bubbleBearing >= userHeading ? bubbleBearing - userHeading : abs(360 - userHeading) + bubbleBearing
            let leftDistance = bubbleBearing < userHeading ? userHeading - bubbleBearing : abs((bubbleBearing - 360) -  userHeading)
            var angle:Double = 0
            
            if bubbleObject.bubblePosition.x != 0.0 {
                
                let kPositionRatio:CGFloat = 1.5
                angle = Double(atan((kPositionRatio * bubbleObject.bubblePosition.y + bubbleObject.bubbleSize.height) / (bubbleObject.bubblePosition.x + bubbleObject.bubbleSize.width/2))) * self.kRadiansToDegrees
            }
            
            if rightDistance >= leftDistance {
                
                //point arrow left and move to left side
                let circleAngle = 180 - angle
                let rotationAngle = -90 - angle
                
                let x = self.frame.size.width / 2 + (self.frame.size.width / 2) * CGFloat(cos(circleAngle * self.kDegreesToRadians))
                let y = self.frame.size.height / 2 + (self.frame.size.height / 2) * CGFloat(sin(circleAngle * self.kDegreesToRadians))

                imageView.center = CGPointMake(x, y)
                imageView.transform = CGAffineTransformMakeRotation(CGFloat(rotationAngle * self.kDegreesToRadians))
                
            } else {
                
                //point arrow right and move to right side
                let circleAngle = -angle
                let rotationAngle = 90 - angle
                
                let x = self.frame.size.width / 2 + (self.frame.size.width / 2) * CGFloat(cos(circleAngle * self.kDegreesToRadians))
                let y = self.frame.size.height / 2 + (self.frame.size.height / 2) * CGFloat(sin(circleAngle * self.kDegreesToRadians))
                
                imageView.center = CGPointMake(x, y)
                imageView.transform = CGAffineTransformMakeRotation(CGFloat(rotationAngle * self.kDegreesToRadians))
            }
            
            let kBubbleYOffset:CGFloat = 0.2
            var shouldArrowBeShown = min(rightDistance, leftDistance) > cameraAngle / 2
            shouldArrowBeShown = shouldArrowBeShown || bubbleObject.bubblePosition.y < -1 - kBubbleYOffset
            shouldArrowBeShown = shouldArrowBeShown || bubbleObject.bubblePosition.y > 1 - kBubbleYOffset
            
            imageView.hidden = !shouldArrowBeShown
        }
    }
    
}
