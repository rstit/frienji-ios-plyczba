//
//  AvatarSelectionCollectionViewController.swift
//  Frienji
//
//  Created by bolek on 02.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AvatarSelectionViewController: UIViewController, StoryboardLoad, UICollectionViewDelegate, UICollectionViewDataSource
{
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "AvatarSelectionViewController"
    
    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var infoLbl:UILabel!
    @IBOutlet var collectionView:UICollectionView!
    
    var viewModel : AvatarSelectionViewModel!
    
    //MARK: - View lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = AvatarSelectionViewModel()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.initControls()
    }
    
    //MARK: - IBActions
    
    @IBAction func goBackClicked(sender:UIButton)
    {
        if self.presentingViewController?.presentedViewController == self {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    //MARK: - Public methods
    
    func setEntryType(entryType: AvatarType)
    {
        self.viewModel.setEntryType(entryType)
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if identifier == R.segue.avatarSelectionViewController.signUp.identifier {
            return !isModal
        } else {
            return true
        }
    }

    //MARK: - Private methods
    
    private func initControls()
    {
        self.titleLbl.text = Localizations.avatarSelection.titleLbl
        self.infoLbl.text = Localizations.avatarSelection.infoLbl
    }

    // MARK: UICollectionViewDataSource

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.viewModel.getNumberOfItems()
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AvatarSelectionCollectionViewCell.identifier, forIndexPath: indexPath) as! AvatarSelectionCollectionViewCell
    
        cell.configureCellFor(self.viewModel.getItemForIndexPath(indexPath), entryType: self.viewModel.entryType)
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        self.collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        let cell = self.collectionView.cellForItemAtIndexPath(indexPath) as! AvatarSelectionCollectionViewCell
        
        self.viewModel.storeAvatarInfo(cell.avatarName)

        if isModal {
            let navigation = NavigationHandler(sourceViewController: self)
            navigation.dismissModal(SettingsViewController.self) { input in
                input.avatarSelected?.onNext(Avatar(name: cell.avatarName.emojiEscaped, type: self.viewModel.entryType))
            }
        }
    }
}
