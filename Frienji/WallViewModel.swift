//
//  WallViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Curry
import CoreLocation

enum WallCommand {

    case LoadFrienji(frienji: Frienji)
    case LoadPosts(posts: [Post])
    case SendPost(post: Post)

}

struct WallViewModel {

    let frienjiName: String
    let posts: [Post]

    init(frienjiName: String = "", posts: [Post] = []) {
        self.frienjiName = frienjiName
        self.posts = posts
    }

    func executeCommand(command: WallCommand) -> WallViewModel {
        switch command {
        case let .LoadFrienji(frienji):
            return WallViewModel(frienjiName: frienji.username, posts: posts)
        case let .LoadPosts(posts):
            return WallViewModel(frienjiName: frienjiName, posts: posts)
        case let .SendPost(post):
            return WallViewModel(frienjiName: frienjiName, posts: posts.arrayByInserting(post, atIndex: 0))
        }
    }

}

extension WallViewModel {

    // MARK: - Create observable

    static func create(withProfileLoaded profile: Observable<Frienji>, textSent text: Observable<String>, imageAttached image: BehaviorSubject<UIImage?>, locationAttached location: BehaviorSubject<CLLocation?>, loadMoreTriggered loadMore: Observable<Void>) -> Observable<WallViewModel> {
        return create(withCommands: loadProfileCommand(profile), loadPostsCommand(profile, loadMore: loadMore), sendPostCommand(profile, text: text, image: image, location: location))
    }

    static func create(withProfileLoaded profile: Observable<Frienji>, postLoaded post: Observable<Post>, textSent text: Observable<String>, imageAttached image: BehaviorSubject<UIImage?>, locationAttached location: BehaviorSubject<CLLocation?>, loadMoreTriggered loadMore: Observable<Void>) -> Observable<WallViewModel> {
        return create(withCommands: loadProfileCommand(profile), loadPostsCommand(profile, post: post, loadMore: loadMore), sendCommentCommand(profile, post: post, text: text, image: image, location: location))
    }

    static func create(withCommands commands: Observable<WallCommand>...) -> Observable<WallViewModel> {
        return commands.toObservable().merge()
            .scan(WallViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .startWith(WallViewModel())
            .shareReplay(1)
    }

    // MARK: - Commands

    private static func loadProfileCommand(profile: Observable<Frienji>) -> Observable<WallCommand> {
        return profile.map(WallCommand.LoadFrienji)
    }

    private static func loadPostsCommand(profile: Observable<Frienji>, loadMore: Observable<Void>) -> Observable<WallCommand> {
        return profile
            .flatMap { FrienjiApi.sharedInstance.getFrienjisPosts($0, loadMore: loadMore) }
            .map(WallCommand.LoadPosts)
    }

    private static func loadPostsCommand(profile: Observable<Frienji>, post: Observable<Post>, loadMore: Observable<Void>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, post) { ($0, $1) }
            .flatMap { FrienjiApi.sharedInstance.getPostComments($0, post: $1, loadMore: loadMore) }
            .map(WallCommand.LoadPosts)
    }

    private static func sendPostCommand(profile: Observable<Frienji>, text: Observable<String>, image: BehaviorSubject<UIImage?>, location: BehaviorSubject<CLLocation?>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, text) { curry(FrienjiApi.sharedInstance.sendPost)($0)($1) }
            .withLatestFrom(image) { sendPost, image in
                sendPost(image)
            }
            .withLatestFrom(location) { sendPost, location in
                sendPost(location?.coordinate.latitude)(location?.coordinate.longitude)
            }
            .flatMap { $0 }
            .doOnNext { _ in
                image.onNext(nil)
                location.onNext(nil)
            }
            .map(WallCommand.SendPost)
    }

    private static func sendCommentCommand(profile: Observable<Frienji>, post: Observable<Post>, text: Observable<String>, image: BehaviorSubject<UIImage?>, location: BehaviorSubject<CLLocation?>) -> Observable<WallCommand> {
        return Observable.combineLatest(profile, post, text) { curry(FrienjiApi.sharedInstance.sendComment)($0)($1)($2) }
            .withLatestFrom(image) { sendPost, image in
                sendPost(image)
            }
            .withLatestFrom(location) { sendPost, location in
                sendPost(location?.coordinate.latitude)(location?.coordinate.longitude)
            }
            .flatMap { $0 }
            .doOnNext { _ in
                image.onNext(nil)
                location.onNext(nil)
            }
            .map(WallCommand.SendPost)
    }

}
