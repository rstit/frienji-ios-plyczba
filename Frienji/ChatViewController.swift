//
//  ChatViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 20/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import RxSwift
import RxCocoa
import SDWebImage
import Curry

class ChatViewController: JSQMessagesViewController, HasDisposeBag {

    static let kSystemFont = UIFont.systemFontOfSize(14.0)

    let messages = Variable<[JSQMessage]>([])
    let sentTrigger = PublishSubject<String?>()

    let disposeBag = DisposeBag()

    var conversation: Conversation?
    var sender: Frienji? = Settings.sharedInstance.userFrienji
    var receiver: Frienji? {
        return conversation?.receiver
    }

    let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImageWithColor(UIColor.appMessageLightEcruColor())
    let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImageWithColor(UIColor.appMessageLightGreenColor())

    override func viewDidLoad() {
        super.viewDidLoad()

        if let sender = sender {
            senderId = "\(sender.userId)"
            senderDisplayName = sender.username
        } else {
            senderId = ""
            senderDisplayName = ""
        }

        automaticallyScrollsToMostRecentMessage = true
        configureInputToolbar()
        configureCollectionView()
        collectionView.layoutIfNeeded()

        createBindings()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        updateCollectionViewInsets()
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Chat) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Chat) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Chat)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    func configureInputToolbar() {
        inputToolbar.translucent = false
        inputToolbar.contentView.backgroundColor = UIColor.yellowColor()
        inputToolbar.contentView.rightBarButtonItem.setTitleColor(UIColor.appPurpleColor(), forState: .Normal)
        inputToolbar.contentView.textView.font = ChatViewController.kSystemFont
    }

    func configureCollectionView() {
        collectionView.collectionViewLayout.incomingAvatarViewSize = .zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = .zero
        collectionView.collectionViewLayout.messageBubbleFont = ChatViewController.kSystemFont
        collectionView.collectionViewLayout.springinessEnabled = false
        collectionView.backgroundColor = UIColor.appLightCyanColor()
    }

    func createBindings() {
        guard let conversation = conversation, sender = sender, receiver = receiver else {
            return
        }

        // Load more trigger
        let contentOffset: Observable<CGPoint?> = self.collectionView.rx_observeWeakly(CGPoint.self, "contentOffset", options: [.New, .Initial])
        let loadMore = contentOffset
            .flatMap { contentOffset -> Observable<Void> in
                guard let contentOffset = contentOffset else {
                    return Observable.empty()
                }

                return contentOffset.y <= 0 ? Observable.just() : Observable.empty()
            }

        let notificationHandler = NotificationHandler()

        let attachmentHandler = AttachmentHandler(targetViewController: self)
        let imageAttached = attachmentHandler.image
        let locationAttached = attachmentHandler.location

        // Image attachments
        imageAttached
            .bindTo(inputToolbar.rx_imageAttachment)
            .addDisposableTo(disposeBag)

        // Location attachment
        locationAttached
            .flatMap { $0?.mapSnapshot.map(Optional.init) ?? Observable.just(nil) }
            .bindTo(inputToolbar.rx_locationAttachment)
            .addDisposableTo(disposeBag)

        // API drivers
        let fetchMessages = FrienjiApi.sharedInstance.getConversationMessages(conversation, loadMore: loadMore)
            .showAlertOnApiError(self)
            .asDriver(onErrorJustReturn: [])

        // Initialize commands
        let loadCommand = fetchMessages.asObservable()
            .map(ChatCommand.Load)
        let sendCommand = sentTrigger
            .map(curry(FrienjiApi.sharedInstance.sendMessage)(conversation))
            .withLatestFrom(imageAttached) { sendMessage, image in
                sendMessage(image)
            }
            .withLatestFrom(locationAttached) { sendMessage, location in
                sendMessage(location?.coordinate.latitude)(location?.coordinate.longitude)
            }
            .flatMap { $0 }
            .doOnNext { _ in
                imageAttached.onNext(nil)
                locationAttached.onNext(nil)
            }
            .showAlertOnApiError(self)
            .map(ChatCommand.Send)
        let receiveCommand = notificationHandler.messageByAuthor(receiver)
            .showAlertOnApiError(self)
            .catchError { _ in notificationHandler.messageByAuthor(receiver) }
            .map(ChatCommand.Receive)

        // Initialize view model
        let viewModel = Observable.of(loadCommand, sendCommand, receiveCommand)
            .merge()
            .scan(ChatViewModel(sender: sender, receiver: receiver)) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)

        // Bind view model
        viewModel
            .map { [unowned self] viewModel in
                viewModel.messages.flatMap { message in
                    JSQMessage.messagesFromMessage(message, sender: viewModel.sender, receiver: viewModel.receiver) { self.collectionView.reloadData() }
                }
            }
            .bindTo(messages)
            .addDisposableTo(disposeBag)
        viewModel
            .map { $0.receiver.username }
            .startWith("")
            .bindTo(navigationItem.rx_title)
            .addDisposableTo(disposeBag)

        messages.asObservable().map { _ in return }
            .doOnNext(collectionView.reloadData)
            .withLatestFrom(sentTrigger) { _ in true }
            .subscribeNext(scrollToBottomAnimated)
            .addDisposableTo(disposeBag)

        messages.asObservable().take(2) // first is an initial empty array, second is last page from api
            .subscribeNext { [unowned self] _ in
                self.scrollToBottomAnimated(false)
            }
            .addDisposableTo(disposeBag)
    }

    func updateCollectionViewInsets() {
        setTableViewInsetsTopValue(topLayoutGuide.length + topContentAdditionalInset, bottomValue: CGRectGetMaxY(collectionView.frame) - CGRectGetMinY(inputToolbar.frame))
    }

    func setTableViewInsetsTopValue(top: CGFloat, bottomValue bottom: CGFloat) {
        let insets = UIEdgeInsetsMake(top, 0.0, bottom, 0.0)
        collectionView.contentInset = insets
        collectionView.scrollIndicatorInsets = insets
    }

    override func didPressSendButton(button: UIButton?, withMessageText text: String?, senderId: String?, senderDisplayName: String?, date: NSDate?) {
        sentTrigger.onNext(text != "" ? text : nil)
        finishSendingMessageAnimated(true)
    }

    override func didPressAccessoryButton(sender: UIButton!) {
        // JSQMessagesViewController requires to implement this method
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.value.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath)
        if let cell = cell as? JSQMessagesCollectionViewCell where cell.textView != nil {
            cell.textView.textColor = UIColor.blackColor()
        }

        return cell
    }

    override func collectionView(collectionView: JSQMessagesCollectionView?, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData? {
        return messages.value[indexPath.row]
    }

    override func collectionView(collectionView: JSQMessagesCollectionView?, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource? {
        return messages.value[indexPath.row].senderId == senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAtIndexPath indexPath: NSIndexPath!) {
        let message = messages.value[indexPath.row]
        guard let cellFrame = collectionView.cellForItemAtIndexPath(indexPath)?.frame else {
            return
        }
        
        switch message.media {
        case let mediaItem as JSQPhotoMediaItem:
            popupImage(mediaItem.image, frame: cellFrame)
        case let mediaItem as JSQLocationMediaItem:
            mediaItem.location.mapSnapshot
                .subscribeNext { [unowned self] in
                    self.popupImage($0, frame: cellFrame)
                }
                .addDisposableTo(disposeBag)
        default:
            return
        }
    }
    
    func popupImage(image: UIImage, frame: CGRect) {
        let fullscreenImage = UIImageView(image: image)
        fullscreenImage.frame = frame
        fullscreenImage.showFullscreen()
        fullscreenImage.fullscreenImageView?.rx_gesture(.Tap)
            .subscribeNext { _ in
                fullscreenImage.hideFullscreen()
            }
            .addDisposableTo(disposeBag)
    }

}

extension ChatViewController: HasAttachment {

    var attachmentRequested: Observable<Void> {
        return inputToolbar.contentView.leftBarButtonItem.rx_tap.asObservable()
    }

}

// MARK: - Message helpers

private extension JSQMessage {

    class func messagesFromMessage(message: Message, sender: Frienji, receiver: Frienji, fetchCompletion: (() -> Void)? = nil) -> [JSQMessage] {
        let id = message.isAuthor ? sender.userId : receiver.userId
        let username = message.isAuthor ? sender.username : receiver.username

        var messages = [JSQMessage]()
        if let text = message.content {
            messages.append(
                JSQMessage(senderId: "\(id)",
                    senderDisplayName: username,
                    date: message.createdAt,
                    text: text
                )
            )
        }
        if let imageUrlString = message.attachmentImageUrl, imageUrl = NSURL(string: imageUrlString) where imageUrlString != "" {
            let photoMediaItem = JSQPhotoMediaItem()
            photoMediaItem.setImageFromUrl(imageUrl, completion: fetchCompletion)
            messages.append(
                JSQMessage(senderId: "\(id)",
                    senderDisplayName: username,
                    date: message.createdAt,
                    media: photoMediaItem
                )
            )
        }
        if let latitude = message.latitude, longitude = message.longitude {
            let locationMediaItem = JSQLocationMediaItem()
            locationMediaItem.setLocation(CLLocation(latitude: latitude, longitude: longitude), withCompletionHandler: fetchCompletion)
            messages.append(
                JSQMessage(
                    senderId: "\(id)",
                    senderDisplayName: username,
                    date: message.createdAt,
                    media: locationMediaItem
                )
            )
        }

        return messages
    }

}

private extension JSQPhotoMediaItem {

    func setImageFromUrl(imageUrl: NSURL, completion: (() -> Void)? = nil) {
        SDWebImageManager.sharedManager()
            .downloadImageWithURL(imageUrl,
                options: SDWebImageOptions(rawValue: UInt(0)),
                progress: { _, _ in },
                completed: { [weak self] image, _, cacheType, finished, _ in
                    if let image = image where finished {
                        self?.image = image
                        if cacheType == SDImageCacheType.None {
                            completion?()
                        }
                    }
                }
            )
    }

}
