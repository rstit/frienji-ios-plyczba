precision mediump float;

uniform sampler2D Texture;
varying vec2 CameraTextureCoord;

void main() {
    gl_FragColor = texture2D(Texture, CameraTextureCoord);
}