//
//  AvatarGroupTableViewController.swift
//  Frienji
//
//  Created by bolek on 02.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AvatarGroupViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "AvatarGroupViewController"
    
    @IBOutlet weak var emojisButton: UIButton!
    @IBOutlet weak var familiarsButton: UIButton!
    @IBOutlet weak var creatures: UIButton!

    //MARK: - IBActions
    
    @IBAction func goBackClicked(sender:UIButton)
    {
        if self.presentingViewController?.presentedViewController == self {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    //MARK: 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "avatarGroupSegue") {
            guard let destinationViewCont = segue.destinationViewController as? AvatarSelectionViewController else {return}
            
            guard let button = sender as? UIButton else {return}
            if button == emojisButton {
                destinationViewCont.setEntryType(AvatarType.Emoji)
            } else if button == familiarsButton {
                destinationViewCont.setEntryType(AvatarType.Image)
            } else {
                destinationViewCont.setEntryType(AvatarType.AnimatedImage)
            }
        }
    }
}
