//
//  WallCommentsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 05/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

class WallCommentsViewController: WallViewController {

    // MARK: - Outlets

    @IBOutlet weak var post: PostView!

    // MARK: - Observables

    var postLoaded = ReplaySubject<Post>.create(bufferSize: 1)
    override var viewModel: Observable<WallViewModel> {
        return WallViewModel.create(
            withProfileLoaded: input.loaded,
            postLoaded: postLoaded,
            textSent: textSent,
            imageAttached: attachmentHandler.image,
            locationAttached: attachmentHandler.location,
            loadMoreTriggered: tableView.loadMoreTrigger
        )
    }

    // MARK: - Bindings

    override func createBindings() {
        super.createBindings()

        postLoaded.bindTo(post.loaded).addDisposableTo(disposeBag)
    }

}
