//
//  CALayer+Border.swift
//  Frienji
//
//  Created by Piotr Łyczba on 07/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

extension CALayer {
    var borderUIColor: UIColor {
        set {
            borderColor = newValue.CGColor
        }
        
        get {
            guard let borderColor = self.borderColor else {
                return UIColor.clearColor()
            }
            return UIColor(CGColor: borderColor)
        }
    }
}
