//
//  UITableView+LoadMore.swift
//  Frienji
//
//  Created by Piotr Łyczba on 18/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

extension UIScrollView {

    private struct OffsetProperties {
        static let kStartLeadingOffset: CGFloat = 20.0
    }

    var loadMoreTrigger: Observable<Void> {
        return rx_contentOffset.flatMap { contentOffset in
            contentOffset.y + self.frame.size.height + OffsetProperties.kStartLeadingOffset > self.contentSize.height
                ? Observable.just()
                : Observable.empty()
        }
    }

}
