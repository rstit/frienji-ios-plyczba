//
//  PostsViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 03/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import JSQMessagesViewController

private var kKeyValueObservingContext = 0

class PostsViewController: UIViewController, JSQMessagesInputToolbarDelegate, JSQMessagesKeyboardControllerDelegate, UITextViewDelegate {

    var topContentAdditionalInset: CGFloat = 0
    var isObserving = false
    var textViewWasFirstResponderDuringInteractivePop = false
    var automaticallyScrollsToMostRecentMessage = true

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputToolbar: JSQMessagesInputToolbar!
    @IBOutlet weak var toolbarHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var toolbarBottomLayoutGuide: NSLayoutConstraint!

    var keyboardController: JSQMessagesKeyboardController!
    var currentInteractivePopGestureRecognizer: UIGestureRecognizer!
    var snapshotView: UIView!

    var currentlyComposedText: String {
        let textView = inputToolbar.contentView.textView
        // auto-accept any auto-correct suggestions
        textView.inputDelegate?.selectionWillChange(textView)
        textView.inputDelegate?.selectionDidChange(textView)

        return textView.text
    }

    // MARK: - View controller's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureInputToolbar()
        
        topContentAdditionalInset = 0.0
        updateTableViewInsets()

        keyboardController = JSQMessagesKeyboardController(textView: inputToolbar.contentView.textView, contextView: view, panGestureRecognizer: tableView.panGestureRecognizer, delegate: self)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        addObservers()
        addActionToInteractivePopGestureRecognizer(true)
        keyboardController.beginListeningForKeyboard()
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        addActionToInteractivePopGestureRecognizer(false)
        removeObservers()
        keyboardController.endListeningForKeyboard()
    }

    deinit {
        removeObservers()
        keyboardController.endListeningForKeyboard()
    }

    // MARK: - Posts view controller

    func configureInputToolbar() {
        toolbarHeightConstraint.constant = inputToolbar.preferredDefaultHeight
        inputToolbar.delegate = self
        inputToolbar.contentView.textView.placeHolder = NSBundle.jsq_localizedStringForKey("new_message")
        inputToolbar.contentView.textView.accessibilityLabel = NSBundle.jsq_localizedStringForKey("new_message")
        inputToolbar.contentView.textView.delegate = self
    }

    func scrollToTopAnimated(animated: Bool) {
        if tableView.numberOfSections == 0 || tableView.numberOfRowsInSection(0) == 0 {
            return
        }
        tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
    }

    func didPressSendButton(sender: UIButton, withText text: String, date: NSDate) {
        assert(false, "Error! required method not implemented in subclass. Need to implement \(#function)")
    }

    func didPressAccessoryButton(sender: UIButton) {
        assert(false, "Error! required method not implemented in subclass. Need to implement \(#function)")
    }

    func finishSendingMessage() {
        finishSendingMessageAnimated(true)
    }

    func finishSendingMessageAnimated(animated: Bool) {
        let textView = inputToolbar.contentView.textView
        textView.text = nil
        textView.undoManager!.removeAllActions()

        inputToolbar.toggleSendButtonEnabled()

        NSNotificationCenter.defaultCenter().postNotificationName(UITextViewTextDidChangeNotification, object: textView)
        tableView.reloadData()

        if automaticallyScrollsToMostRecentMessage {
            scrollToTopAnimated(animated)
        }
    }

    // MARK: - Input toolbar delegate

    func messagesInputToolbar(toolbar: JSQMessagesInputToolbar, didPressLeftBarButton sender: UIButton) {
        if toolbar.sendButtonOnRight {
            self.didPressAccessoryButton(sender)
        }
        else {
            self.didPressSendButton(sender, withText: currentlyComposedText, date: NSDate())
        }
    }

    func messagesInputToolbar(toolbar: JSQMessagesInputToolbar, didPressRightBarButton sender: UIButton) {
        if toolbar.sendButtonOnRight {
            self.didPressSendButton(sender, withText: currentlyComposedText, date: NSDate())
        }
        else {
            self.didPressAccessoryButton(sender)
        }
    }


    // MARK: - Text view delegate

    func textViewDidBeginEditing(textView: UITextView) {
        guard textView == inputToolbar.contentView.textView else {
            return
        }

        textView.becomeFirstResponder()
        if automaticallyScrollsToMostRecentMessage {
            scrollToTopAnimated(true)
        }
    }

    func textViewDidChange(textView: UITextView) {
        guard textView == inputToolbar.contentView.textView else {
            return
        }

        inputToolbar.toggleSendButtonEnabled()
    }

    func textViewDidEndEditing(textView: UITextView) {
        guard textView == inputToolbar.contentView.textView else {
            return
        }

        textView.resignFirstResponder()
    }

    // MARK: - Key-value observing

    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String: AnyObject]?, context: UnsafeMutablePointer<Void>) {
        guard let keyPath = keyPath, object = object as? NSObject, change = change else {
            return
        }

        switch context {
        case &kKeyValueObservingContext:
            guard let oldContentSize = change[NSKeyValueChangeOldKey]?.CGSizeValue(),
                newContentSize = change[NSKeyValueChangeNewKey]?.CGSizeValue()
                where object == inputToolbar.contentView.textView && keyPath == "contentSize"
                else {
                fallthrough
            }
            let dy: CGFloat = newContentSize.height - oldContentSize.height
            adjustInputToolbar(forComposerTextViewContentSizeChange: dy)
            updateTableViewInsets()
        default:
            return
        }
    }

    // MARK: - Input toolbar utilities

    func inputToolbarHasReachedMaximumHeight() -> Bool {
        return CGRectGetMinY(inputToolbar.frame) == (topLayoutGuide.length + topContentAdditionalInset)
    }

    func adjustInputToolbar(forComposerTextViewContentSizeChange dy: CGFloat) {
        let contentSizeIsIncreasing = dy > 0
        if inputToolbarHasReachedMaximumHeight() {
            let contentOffsetIsPositive = inputToolbar.contentView.textView.contentOffset.y > 0
            if contentSizeIsIncreasing || contentOffsetIsPositive {
                scrollComposerTextViewToBottomAnimated(true)
                return
            }
        }
        let toolbarOriginY = CGRectGetMinY(inputToolbar.frame)
        let newToolbarOriginY = toolbarOriginY - dy
        //  attempted to increase origin.Y above topLayoutGuide
        var dy = dy
        if newToolbarOriginY <= topLayoutGuide.length + topContentAdditionalInset {
            dy = toolbarOriginY - (topLayoutGuide.length + topContentAdditionalInset)
            scrollComposerTextViewToBottomAnimated(true)
        }
        adjustInputToolbarHeightConstraintByDelta(dy)
        updateKeyboardTriggerPoint()
        if dy < 0 {
            scrollComposerTextViewToBottomAnimated(false)
        }
    }

    func adjustInputToolbarHeightConstraintByDelta(dy: CGFloat) {
        let proposedHeight: CGFloat = toolbarHeightConstraint.constant + dy
        var finalHeight: CGFloat = max(proposedHeight, inputToolbar.preferredDefaultHeight)
        if inputToolbar.maximumHeight != UInt(NSNotFound) {
            finalHeight = min(finalHeight, CGFloat(inputToolbar.maximumHeight))
        }
        if toolbarHeightConstraint.constant != finalHeight {
            toolbarHeightConstraint.constant = finalHeight
            view.setNeedsUpdateConstraints()
            view.layoutIfNeeded()
        }
    }

    func scrollComposerTextViewToBottomAnimated(animated: Bool) {
        let textView = inputToolbar.contentView.textView
        let contentOffsetToShowLastLine = CGPointMake(0.0, textView.contentSize.height - CGRectGetHeight(textView.bounds))
        if !animated {
            textView.contentOffset = contentOffsetToShowLastLine
            return
        }
        UIView.animateWithDuration(0.01, delay: 0.01, options: .CurveLinear, animations: { () -> Void in
            textView.contentOffset = contentOffsetToShowLastLine
            }, completion: { _ in })
    }

    // MARK: - Keyboard controller delegate

    func keyboardController(keyboardController: JSQMessagesKeyboardController, keyboardDidChangeFrame keyboardFrame: CGRect) {
        if !inputToolbar.contentView.textView.isFirstResponder() && toolbarBottomLayoutGuide.constant == 0.0 {
            return
        }
        var heightFromBottom: CGFloat = CGRectGetMaxY(tableView.frame) - CGRectGetMinY(keyboardFrame)
        heightFromBottom = max(0.0, heightFromBottom)
        setToolbarBottomLayoutGuideConstant(heightFromBottom)
    }

    func setToolbarBottomLayoutGuideConstant(constant: CGFloat) {
        toolbarBottomLayoutGuide.constant = constant
        view.setNeedsUpdateConstraints()
        view.layoutIfNeeded()
        updateTableViewInsets()
    }

    func updateKeyboardTriggerPoint() {
        keyboardController.keyboardTriggerPoint = CGPointMake(0.0, CGRectGetHeight(inputToolbar.bounds))
    }

    // MARK: Table view utilities

    func updateTableViewInsets() {
        setTableViewInsetsTopValue(topLayoutGuide.length + topContentAdditionalInset, bottomValue: CGRectGetMaxY(tableView.frame) - CGRectGetMinY(inputToolbar.frame))
    }

    func setTableViewInsetsTopValue(top: CGFloat, bottomValue bottom: CGFloat) {
        let insets = UIEdgeInsetsMake(top, 0.0, bottom, 0.0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
    }

    // MARK: - Utilities

    func addObservers() {
        if isObserving {
            return
        }
        inputToolbar.contentView.textView.addObserver(self, forKeyPath: "contentSize", options: [.Old, .New], context: &kKeyValueObservingContext)
        isObserving = true
    }

    func removeObservers() {
        if !isObserving {
            return
        }
        inputToolbar.contentView.textView.removeObserver(self, forKeyPath: "contentSize", context: &kKeyValueObservingContext)
        isObserving = false
    }

    func addActionToInteractivePopGestureRecognizer(addAction: Bool) {
        if currentInteractivePopGestureRecognizer != nil {
            currentInteractivePopGestureRecognizer.removeTarget(nil, action: #selector(handleInteractivePopGestureRecognizer))
            currentInteractivePopGestureRecognizer = nil
        }
        if addAction {
            navigationController?.interactivePopGestureRecognizer?.addTarget(self, action: #selector(handleInteractivePopGestureRecognizer))
            currentInteractivePopGestureRecognizer = navigationController?.interactivePopGestureRecognizer
        }
    }

    // MARK: - Gesture recognizers

    func handleInteractivePopGestureRecognizer(gestureRecognizer: UIGestureRecognizer) {
        switch gestureRecognizer.state {
        case .Began:
            textViewWasFirstResponderDuringInteractivePop = inputToolbar.contentView.textView.isFirstResponder()
            keyboardController.endListeningForKeyboard()
        case .Changed:
            break
        case .Cancelled, .Ended, .Failed:
            keyboardController.beginListeningForKeyboard()
            if textViewWasFirstResponderDuringInteractivePop {
                inputToolbar.contentView.textView.becomeFirstResponder()
            }
        default:
            break
        }
    }

}
