//
//  ZooViewController.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SDWebImage
import CenterAlignedCollectionViewFlowLayout

typealias ZooSectionModel = AnimatableSectionModel<String, Frienji>
typealias ZooCollectionDataSource = RxCollectionViewSectionedAnimatedDataSource<ZooSectionModel>

class ZooViewController: UICollectionViewController {

    // MARK: - UI Controls

    var backButton: UIBarButtonItem {
        return UIBarButtonItem(title: Localizations.zoo.backBtn, style: .Plain, target: self, action: #selector(goBack(_:)))
    }

    var inboxButton: UIBarButtonItem {
        return UIBarButtonItem(title: Localizations.zoo.inboxBtn, style: .Plain, target: self, action: #selector(goToInbox(_:)))
    }

    @IBOutlet weak var editZooButton: UIBarButtonItem!

    override var collectionViewLayout: UICollectionViewLayout {
        guard let oldLayout = super.collectionViewLayout as? UICollectionViewFlowLayout else {
            return super.collectionViewLayout
        }
        let layout = CenterAlignedCollectionViewFlowLayout()
        layout.minimumInteritemSpacing = oldLayout.minimumInteritemSpacing
        layout.minimumLineSpacing = oldLayout.minimumLineSpacing
        layout.headerReferenceSize = oldLayout.headerReferenceSize
        layout.itemSize = oldLayout.itemSize

        return layout
    }

    // MARK: - Events

    let itemDeleted = PublishSubject<NSIndexPath>()
    let loadMoreTriggered = PublishSubject<Void>()
    let editingCommited = PublishSubject<Void>()
    let editingCanceled = PublishSubject<Void>()
    var viewModel: Observable<ZooViewModel>!
    let disposeBag = DisposeBag()

    // MARK: - Properties

    override var editing: Bool {
        didSet {
            configureNavigationBar()
            configureToolbar()

            collectionView?.reloadData()
        }
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        configureToolbar()

        collectionView!.collectionViewLayout = collectionViewLayout

        createBindings()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.toolbarHidden = false
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Zoo) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Zoo) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Zoo)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.toolbarHidden = true
    }

    // MARK: - Configure view

    private func configureNavigationBar() {
        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItem = inboxButton
        navigationItem.title = editing ? Localizations.zoo.viewEditingTitle : Localizations.zoo.viewTitle

        if editing {
            navigationItem.leftBarButtonItem?.action = #selector(toggleEditing(_:))
            navigationItem.rightBarButtonItem = nil
        }
    }

    private func configureToolbar() {
        let toolbar = navigationController!.toolbar
        toolbar.barTintColor = UIColor.yellowColor()
        toolbar.setTransparent(!editing)

        if editing {
            edgesForExtendedLayout = .Top
            editZooButton.width = toolbar.bounds.width
            editZooButton.title = Localizations.zoo.editDoneBtn
        } else {
            edgesForExtendedLayout = .All
            editZooButton.width = 0
            editZooButton.title = Localizations.zoo.editBtn
        }
    }

    // MARK: - Bindings

    private func createBindings() {
        viewModel = prepareViewModel()

        bindViewModel()
        bindEditing()
        bindSelection()
    }

    private func prepareViewModel() -> Observable<ZooViewModel> {
        return Observable.of(
            prepareLoad(),
            prepareDelete(),
            prepareCancelEditing()
            )
            .merge()
            .scan(ZooViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)
    }

    private func prepareLoad() -> Observable<ZooCommand> {
        let loadMore = collectionView?.loadMoreTrigger.filter { [unowned self] in !self.editing } ?? Observable.empty()
        return FrienjiApi.sharedInstance.getFrienjis(loadMore).showAlertOnApiError(self)
            .map(ZooCommand.LoadFrienjis)
    }

    private func prepareCancelEditing() -> Observable<ZooCommand> {
        return editingCanceled
            .map(ZooCommand.CancelDelete)
    }

    private func prepareCommitEditing() -> Observable<ZooCommand> {
        return editingCommited
            .map(ZooCommand.CommitDelete)
    }

    private func prepareDelete() -> Observable<ZooCommand> {
        return itemDeleted
            .map { $0.row }
            .map(ZooCommand.DeleteFrienjiAtIndexPath)
    }

    private func prepareDataSource() -> ZooCollectionDataSource {
        collectionView!.dataSource = nil
        let dataSource = ZooCollectionDataSource()
        dataSource.configureCell = { _, collectionView, indexPath, frineji in
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ZooCollectionViewCell.identifier, forIndexPath: indexPath)
            guard let zooCell = cell as? ZooCollectionViewCell else {
                return cell
            }

            zooCell.confireCellForFrienji(frineji, isViewInEditionMode: self.editing)

            return zooCell
        }
        dataSource.supplementaryViewFactory = { _, collectionView, kind, indexPath in
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "ZooHeader", forIndexPath: indexPath)
            guard let zooHeader = header as? ZooCollectionViewHeader else {
                return header
            }

            zooHeader.headerTitle.text = self.editing ? Localizations.zoo.header.editingTitle : Localizations.zoo.header.title

            return zooHeader
        }

        return dataSource
    }

    private func bindViewModel() {
        viewModel
            .map { viewModel in
                [ZooSectionModel(model: "zoo", items: viewModel.frienjis)]
            }
            .bindTo(collectionView!.rx_itemsWithDataSource(prepareDataSource()))
            .addDisposableTo(disposeBag)
    }

    private func bindSelection() {
        collectionView!.rx_modelSelected(Frienji.self)
            .filter { _ in !self.editing }
            .subscribeNext(showWallViewForFrienji)
            .addDisposableTo(disposeBag)
    }

    private func bindEditing() {
        editingCommited
            .withLatestFrom(viewModel) { _, viewModel in
                viewModel.deletedFrienjis
            }
            .flatMap { frienjis in
                FrienjiApi.sharedInstance.deleteFrienjis(frienjis)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    // MARK: - Actions

    func goBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func goToInbox(sender: AnyObject) {
        self.navigationController?.pushViewController(InboxViewController.loadFromStoryboard(), animated: true)
    }

    @IBAction func toggleEditing(sender: UIBarButtonItem) {
        if editing {
            if sender === editZooButton {
                editingCommited.onNext()
            } else {
                editingCanceled.onNext()
            }
        }
        editing = !editing
    }

    @IBAction func deleteItem(sender: UIButton) {
        let buttonPosition = sender.convertPoint(CGPoint(x: sender.bounds.midX, y: sender.bounds.midY), toView: collectionView)
        guard let indexPath = collectionView!.indexPathForItemAtPoint(buttonPosition) else { return }

        itemDeleted.onNext(indexPath)
    }

    func showWallViewForFrienji(frienji: Frienji) {
        performSegueWithIdentifier(R.segue.zooViewController.showFrienji.identifier, sender: Observable.just(frienji))
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let wallPostsViewController = segue.destinationViewController as? WallPostsViewController, frienji = sender as? Observable<Frienji> {
            frienji.bindTo(wallPostsViewController.input.loaded).addDisposableTo(disposeBag)
            wallPostsViewController.view.backgroundColor = UIColor.appCyanColor()

        }
    }

}
