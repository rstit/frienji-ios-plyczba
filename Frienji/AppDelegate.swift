//
//  AppDelegate.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import TimeAgoInWords
import RxSwift

let kDefaultAnimationDuration = 0.33

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var enteringForeground = false

    let notificationTapped = ReplaySubject<AnyObject>.create(bufferSize: 1)

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        enableExternalServices()

        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.makeKeyAndVisible()

        let navCont = UINavigationController()

        self.setupNavigationController(navCont)
        self.setupTimeAgoInWordsStrings()

        self.window?.rootViewController = navCont

        if let userInfo = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] {
            notificationTapped.onNext(userInfo)
        }

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        enteringForeground = true
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        enteringForeground = false
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Open URL

    func application(app: UIApplication, openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        return false
    }

    // MARK: Push notifications

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {

        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")

        let deviceTokenString: String = (deviceToken.description as NSString)
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString(" ", withString: "") as String

        print("device token: \(deviceTokenString), not trimmed: \(deviceToken)")

        Settings.sharedInstance.pushNotificationsToken = deviceTokenString
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject: AnyObject]) {

        print("Notification: \(userInfo)")

        let launchingFromBackground = !(application.applicationState == .Active)
        if enteringForeground && launchingFromBackground {
            notificationTapped.onNext(userInfo)
        }
    }

    // MARK: External services

    private func enableExternalServices() {
        // Fabric
        if ConfigurationsHelper.sharedInstance.isRunningInRelease() {
            Fabric.with([Crashlytics.self])
        }
    }

    private func setupNavigationController(navCont: UINavigationController) {
        if self.checkIfUserIsVerified() {
            self.setupNavigationControllerForLoggedInUser(navCont)
        } else {
            self.setupNavigationControllerForNotLoggedInUser(navCont)
        }
    }

    private func checkIfUserIsVerified() -> Bool {
        return Settings.sharedInstance.userVerified
    }

    private func setupNavigationControllerForLoggedInUser(navCont: UINavigationController) {
        let arController = ArViewController.loadFromStoryboard()

        navCont.viewControllers = [arController]
        navCont.delegate = TransitionDelegate.sharedInstance
    }

    private func setupNavigationControllerForNotLoggedInUser(navCont: UINavigationController) {
        let viewCont = IntroductionViewController.loadFromStoryboard()

        navCont.viewControllers = [viewCont]
        navCont.navigationBarHidden = true
    }

    private func setupTimeAgoInWordsStrings() {
        let railsStrings = [
            "LessThan": Localizations.timeAgo.lessThan,
            "About": Localizations.timeAgo.about,
            "Over": Localizations.timeAgo.over,
            "Almost": Localizations.timeAgo.almost,
            "Seconds": Localizations.timeAgo.seconds,
            "Minute": Localizations.timeAgo.minute,
            "Minutes": Localizations.timeAgo.minutes,
            "Hour": Localizations.timeAgo.hour,
            "Hours": Localizations.timeAgo.hours,
            "Day": Localizations.timeAgo.day,
            "Days": Localizations.timeAgo.days,
            "Months": Localizations.timeAgo.months,
            "Years": Localizations.timeAgo.years,
            ]
        TimeAgoInWordsStrings.updateStrings(railsStrings)
    }

}
