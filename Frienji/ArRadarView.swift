//
//  ArRadarView.swift
//  Traces
//
//  Created by Adam Szeremeta on 12.10.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class ArRadarView: UIView {

    class ArRadarTracesView: UIView {

        private let kUserImage = UIImage(named: "trace_radar_user")!
        private let kTraceImage = UIImage(named: "trace_radar_dot")!

        var location:CLLocation = CLLocation(latitude: 0, longitude: 0)
        var traces = [Frienji]()

        // MARK: Draw

        override func drawRect(rect: CGRect) {

            guard let context = UIGraphicsGetCurrentContext() else {
                return
            }

            //draw traces
            for trace in self.traces {
                CGContextSaveGState(context)
                self.drawTrace(trace, location: self.location, context: context)
                CGContextRestoreGState(context)
            }

            //draw user position
            let center = CGPointMake(rect.size.width / 2, rect.size.height / 2)
            let userImageSize = rect.size.width / 5
            let userImageRect = CGRectMake(center.x - userImageSize / 2, center.y - userImageSize / 2, userImageSize, userImageSize)
            self.kUserImage.drawInRect(userImageRect)
        }

        private func drawTrace(trace:Frienji, location:CLLocation, context:CGContext) {
            //get distance to trace and bearing
            let traceDistance = location.distanceFromLocation(trace.location)
            let traceBearing = LocationManager.sharedInstance.getBearingBetweenTwoPoints(location, point2: trace.location)

            let viewRadius = self.bounds.size.height/2
            let userImageSize = self.bounds.size.width / 5

            //calculate position and angle to rotate
            var yPosition = viewRadius - userImageSize/2 - (viewRadius * CGFloat(traceDistance)) / ArOpenGLView.kMaximumObjectDistance
            yPosition = yPosition < 0 ? userImageSize/4 : yPosition
            let angleToRotate = traceBearing * CGFloat(M_PI) / 180.0

            CGContextTranslateCTM(context, viewRadius, viewRadius)
            CGContextRotateCTM(context, angleToRotate)
            CGContextTranslateCTM(context, -viewRadius, -viewRadius)

            let center = CGPointMake(viewRadius, yPosition)
            let traceImageSize = self.bounds.size.width / 6
            let traceImageRect = CGRectMake(center.x - traceImageSize / 2, center.y - traceImageSize / 2, traceImageSize, traceImageSize)
            self.kTraceImage.drawInRect(traceImageRect)
        }
    }

    private let kBackgroumdImage = UIImage(named: "trace_radar_bg")!

    private var cameraViewAngle:Double = 0
    private var bearing:Double = 0

    private var tracesView:ArRadarTracesView!

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.clipsToBounds = false

        self.tracesView = ArRadarTracesView()
        self.tracesView.backgroundColor = UIColor.clearColor()
        self.tracesView.clipsToBounds = false
        self.addSubviewFullscreen(self.tracesView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Traces

    func updateWithTraces(traces:[Frienji]) {
        self.tracesView.traces = traces

        self.tracesView.setNeedsDisplay()
    }

    func removeTrace(trace:Frienji) {
        if let index = self.tracesView.traces.indexOf(trace) {
            self.tracesView.traces.removeAtIndex(index)
            self.setNeedsDisplay()
        }
    }

    // MARK: Sensors readings

    func setCameraAngle(angle:CGFloat) {
        self.cameraViewAngle = Double(angle)
    }

    func setBearing(bearing:Double) {
        self.bearing = bearing

        let angleToRotate = -(self.bearing) * M_PI / 180.0
        self.tracesView.transform = CGAffineTransformMakeRotation(CGFloat(angleToRotate))
    }

    func setUserLocation(location:CLLocation) {
        self.tracesView.location = location

        self.setNeedsDisplay()
    }

    // MARK: Draw

    override func drawRect(rect: CGRect) {

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        //draw background
        self.kBackgroumdImage.drawInRect(rect)

        //draw angle
        CGContextSaveGState(context)
        self.drawAngleGradient(rect, context: context)
        CGContextRestoreGState(context)
    }

    private func drawAngleGradient(rect: CGRect, context:CGContext) {
        let topPointingAngle = 270.0
        let halfCameraFieldOfView = self.cameraViewAngle/2

        let center = CGPointMake(rect.size.width / 2, rect.size.height / 2)
        let radius: CGFloat = self.bounds.size.height / 2
        let startAngle = CGFloat((topPointingAngle - halfCameraFieldOfView) * M_PI / 180.0)
        let endAngle = CGFloat((topPointingAngle + halfCameraFieldOfView) * M_PI / 180.0)

        let startPoint = CGPointMake(center.x + radius * cos(startAngle), center.y + radius * sin(startAngle))

        CGContextBeginPath(context)
        CGContextMoveToPoint(context, center.x, center.y)
        CGContextAddLineToPoint(context, startPoint.x, startPoint.y)
        CGContextAddArc(context, center.x, center.y, radius, startAngle, endAngle, startAngle > endAngle ? 1 : 0)
        CGContextClosePath(context)
        CGContextClip(context)

        //create gradient
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let componentsCount: Int = 2
        let colorComponents: [CGFloat] = [
            50.0/255, 215.0/255, 165.0/255, 0.5,
            50.0/255, 215.0/255, 165.0/255, 0.1,
            ]
        let locations: [CGFloat] = [0, 1.0]

        if let gradient = CGGradientCreateWithColorComponents(colorSpace, colorComponents, locations, componentsCount) {
            //draw gradient
            CGContextDrawLinearGradient(context, gradient, CGPointMake(center.x, center.y), CGPointMake(center.x, 0), CGGradientDrawingOptions())
        }
    }
    
}
