//
//  Post.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Post {

    let id: Int
    let message: String
    let createdAt: NSDate
    let isAuthor: Bool
    let numberOfComments: Int?
    let numberOfLikes: Int
    let liked: Bool
    let author: Frienji?
    let attachmentUrl: String?
    let latitude: Double?
    let longitude: Double?

}

// MARK: - Fake

extension Post: Fakeable {

    static func fake() -> Post {
        return Post(
            id: faker.number.increasingUniqueId(),
            message: faker.lorem.sentences(amount: 10),
            createdAt: NSDate.randomWithinDaysBeforeToday(356),
            isAuthor: faker.number.randomBool(),
            numberOfComments: faker.number.randomInt(min: 0, max: 5),
            numberOfLikes: faker.number.randomInt(min: 0, max: 5),
            liked: faker.number.randomBool(),
            author: Frienji.fake(),
            attachmentUrl: faker.internet.image(),
            latitude: faker.address.latitude(),
            longitude: faker.address.longitude()
        )
    }

}

extension Post: Decodable {

    static func decode(json: JSON) -> Decoded<Post> {
        let d = curry(Post.init)
        return d
            <^> json <| "id"
            <*> json <| "message" <|> pure("")
            <*> (json <| "created_at" >>- toNSDate)
            <*> json <| "is_author"
            <*> json <|? "number_of_comments"
            <*> json <| "number_of_likes"
            <*> json <| "liked"
            <*> json <|? "author"
            <*> json <|? "attachment_url"
            <*> json <|? ["location", "latitude"]
            <*> json <|? ["location", "longitude"]
    }

}
