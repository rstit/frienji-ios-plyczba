//
//  PhoneNumberViewController.swift
//  Frienji
//
//  Created by bolek on 03.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS
import RxSwift

class PhoneNumberViewController: BaseViewController, StoryboardLoad, UIPickerViewDelegate, UIPickerViewDataSource
{
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "PhoneNumberViewController"
    
    @IBOutlet var titleLbl :UILabel!
    @IBOutlet var infoLbl :UILabel!
    
    @IBOutlet var countryCodeTxt:UITextField!
    @IBOutlet var phoneNumberTxt:UITextField!
    
    var viewModel : PhoneNumberViewModel!
    let disposeBag = DisposeBag()

    //MARK: - View lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = PhoneNumberViewModel()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.initControls()
        self.configureTextFieldsPlaceholder()
        self.configureTextFieldsInputView()
        self.configureTextFieldsInputAcessoryView()
        self.configureTextFieldsBottomBorder()
        
        self.setDefaultCoutryCodeBasedOnLocale()
        
        self.configureBindings()
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.showKeyboard()
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }
    
    //MARK: - Public methods
    
    func setEntryType(entryType:PhoneNumberViewEntryType)
    {
        self.viewModel.entryType = entryType
    }
    
    //MARK: - Private methods
    
    private func initControls()
    {
        self.titleLbl.text = self.viewModel.getViewTitle()
        self.infoLbl.text = Localizations.phoneNumber.infoLbl
        
        self.phoneNumberTxt?.becomeFirstResponder()
    }
    
    private func configureTextFieldsPlaceholder()
    {
        self.countryCodeTxt.attributedPlaceholder = NSAttributedString(string:Localizations.phoneNumber.countryCodePlaceholder,
                                                                        attributes:[NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor()])
        self.phoneNumberTxt.attributedPlaceholder = NSAttributedString(string:Localizations.phoneNumber.phoneNumberPlaceholder,
                                                                        attributes:[NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor()])
    }
    
    private func configureTextFieldsBottomBorder()
    {
        self.countryCodeTxt.setBottomBorder(UIColor.appOrangePlaceholderColor())
        self.phoneNumberTxt.setBottomBorder(UIColor.appOrangePlaceholderColor())
    }
    
    private func configureTextFieldsInputView()
    {
        let picketView = UIPickerView()
        picketView.delegate = self
        picketView.dataSource = self
        self.countryCodeTxt.inputView = picketView
    }
    
    private func configureTextFieldsInputAcessoryView()
    {
        let keyboardInputAccessoryView = UIButton()
        keyboardInputAccessoryView.frame = CGRectMake(0, 0, self.view.frame.size.width,44)
        keyboardInputAccessoryView.setTitle(Localizations.phoneNumber.sendSmsCodeBtn, forState: .Normal)
        keyboardInputAccessoryView.backgroundColor = UIColor.appPaleRoseColor()
        keyboardInputAccessoryView.titleLabel?.font = UIFont.latoRegulatWithSize(17)
        keyboardInputAccessoryView.addTarget(self, action: #selector(sendSmsCodeClicked), forControlEvents: .TouchUpInside)
        
        self.countryCodeTxt.inputAccessoryView = keyboardInputAccessoryView
        self.phoneNumberTxt.inputAccessoryView = keyboardInputAccessoryView
    }
    
    private func setDefaultCoutryCodeBasedOnLocale()
    {
        self.countryCodeTxt.text = self.viewModel.getTitleForCoutryCodePickerRow(self.viewModel.getDefaultCoutryCodeBasedOnLocale())
        (self.countryCodeTxt.inputView as! UIPickerView).selectRow(self.viewModel.getDefaultCoutryCodeBasedOnLocale(), inComponent: 0, animated: false)
    }
    
    func configureBindings()
    {
        self.phoneNumberTxt.rx_text.asObservable().bindNext { [unowned self](phoneNumber) in
            self.viewModel.phoneNumber = phoneNumber
        }.addDisposableTo(self.disposeBag)
    }
    
    private func showKeyboard()
    {
        self.phoneNumberTxt.becomeFirstResponder()
    }

    private func pushSmsCodeViewController()
    {
        let viewCont = SmsCodeViewController.loadFromStoryboard()
        self.navigationController?.pushViewController(viewCont, animated: true)
    }
    
    private func createAccountAndSendSmsCode()
    {
        self.viewModel.createAccountAndSendSmsCode(self) { (success) in
            if(success)
            {
                self.pushSmsCodeViewController()
            }
        }
    }
    
    private func sendSmsCode()
    {
        self.viewModel.sendSmsCode(self) { (success) in
            if(success)
            {
                self.pushSmsCodeViewController()
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func goBackClicked(sender:UIButton)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func sendSmsCodeClicked(sender:UIButton)
    {
        if(self.countryCodeTxt.isFirstResponder())
        {
            self.phoneNumberTxt.becomeFirstResponder()
            return
        }
        
        self.viewModel.isPhoneNumberValid() { (isValid) in
            if(!isValid)
            {
                UIView.shakeViewHorizontally(self.phoneNumberTxt, times: 5, distance: 4)
                UIAlertUtils.showAlertWithTitle(Localizations.phoneNumber.invalidPhoneNumberAllertTitle, message: Localizations.phoneNumber.invalidPhoneNumberAllertMessage(self.viewModel.countryName), fromController: self, showCompletion: nil)
                
                return
            }
            
            self.dismissKeyboard()
            
            self.viewModel.storeUserInfo()
            
            if(self.viewModel.entryType == PhoneNumberViewEntryType.SignUp && !self.viewModel.wasRegistractionSuccessfull)
            {
                self.createAccountAndSendSmsCode()
            }
            else
            {
                self.sendSmsCode()
            }
        }
    }
    
    //MARK: - UIPickerViewDataSource
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
        return self.viewModel.getNumberOfComponentsForCoutryCodePicker
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return self.viewModel.getNumberOfRowsForCoutryCodePicker(component)
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return self.viewModel.getTitleForCoutryCodePickerRow(row)
    }
    
    //MARK: - UIPickerViewDelegate
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        self.countryCodeTxt.text = self.viewModel.updateCoutryCode(NSLocale.ISOCountryCodes()[row])
    }
}
