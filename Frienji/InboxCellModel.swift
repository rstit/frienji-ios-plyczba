//
//  InboxCellModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 16/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import TimeAgoInWords

struct InboxCellModel {

    let authorName: String
    let createdAt: String
    let details: String
    let avatar: UIImage?
    let isRead: Bool

    init(authorName: String = "", createdAt: String = "", details: String = "", avatar: UIImage? = nil, isRead: Bool = false) {
        self.authorName = authorName
        self.createdAt = createdAt
        self.details = details
        self.avatar = avatar
        self.isRead = isRead
    }

    func executeCommand(command: InboxCellCommand) -> InboxCellModel {
        switch command {
        case let .Load(conversation):
            return InboxCellModel(
                authorName: conversation.receiver.username,
                createdAt: conversation.lastMessage?.createdAt.timeAgoInWords() ?? "",
                details: conversation.lastMessage?.content ?? "",
                avatar: conversation.receiver.avatar.avatarImage,
                isRead: conversation.unreadCount == 0
            )
        case .MarkAsRead:
            return InboxCellModel(
                authorName: authorName,
                createdAt: createdAt,
                details: details,
                avatar: avatar,
                isRead: true
            )
        }
    }

}

enum InboxCellCommand {
    case Load(conversation: Conversation)
    case MarkAsRead
}
