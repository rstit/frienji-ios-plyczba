//
//  CatchViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 06/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

struct PreviewInput: Input {
    let loaded = ReplaySubject<Frienji>.create(bufferSize: 1)
}

class PreviewViewController: UIViewController, HasInput {

    // MARK: - Outlets

    @IBOutlet weak var profile: ProfileView!
    @IBOutlet weak var discardButton: UIButton!
    @IBOutlet weak var keepButton: UIButton!

    // MARK: - Observables

    var input = PreviewInput()

    let disposeBag = DisposeBag()

    // MARK: - Properties

    var viewModel: PreviewViewModel!

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        input.loaded.bindTo(profile.profileLoaded).addDisposableTo(disposeBag)

        viewModel = PreviewViewModel(navigationHandler: NavigationHandler(sourceViewController: self))

        input.loaded.map(Optional.init).bindTo(viewModel.frienji).addDisposableTo(disposeBag)
        keepButton.rx_tap.bindTo(viewModel.catched).addDisposableTo(disposeBag)
        discardButton.rx_tap.bindTo(viewModel.rejected).addDisposableTo(disposeBag)
    }

}
