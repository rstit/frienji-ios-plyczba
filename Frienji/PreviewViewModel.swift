//
//  PreviewViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 12/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

class PreviewViewModel {

    var frienji = Variable<Frienji?>(nil)

    let catched = PublishSubject<Void>()
    let rejected = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    init(navigationHandler: NavigationHandler) {
        // Navigation
        catched
            .flatMap { [unowned self] in
                self.frienji.asObservable()
            }
            .bindTo(navigationHandler.modalDismissed(ExplorationViewController.self)) { input, frienji in
                if let frienji = frienji {
                    input.catched.onNext(frienji)
                }
            }
            .addDisposableTo(disposeBag)
        rejected
            .flatMap { [unowned self] in
                self.frienji.asObservable()
            }
            .bindTo(navigationHandler.modalDismissed(ExplorationViewController.self)) { input, frienji in
                if let frienji = frienji {
                    input.rejected.onNext(frienji)
                }
            }
            .addDisposableTo(disposeBag)
    }

}
