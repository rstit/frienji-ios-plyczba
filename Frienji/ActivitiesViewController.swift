//
//  ActivitiesViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ActivitiesViewController: UITableViewController {

    @IBOutlet weak var dismissButton: UIBarButtonItem!

    var viewModel: ActivitiesViewModel!

    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = ActivitiesViewModel(navigationHandler: NavigationHandler(sourceViewController: self))
        createBindings()
        viewModel.loadInitialized.onNext()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.UserActivities) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.UserActivities) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.UserActivities)
            }
            coachmark.showWithAnimationInContainer()
        }
    }
    
    func createBindings() {
        // Table view
        tableView.dataSource = nil
        viewModel.activities.asObservable()
            .bindTo(tableView.rx_itemsWithCellIdentifier(R.reuseIdentifier.activityCell.identifier)) { (row, activity, cell: ActivityCell) in
                cell.configure(activity)
            }
            .addDisposableTo(disposeBag)

        // Load more
        tableView.loadMoreTrigger
            .bindTo(viewModel.loadMoreTriggered)
            .addDisposableTo(disposeBag)

        // Navigation
        dismissButton.rx_tap
            .bindTo(viewModel.dismissTapped)
            .addDisposableTo(disposeBag)

        // Errors
        viewModel.apiError
            .showAlertOnApiError(self)
            .catchErrorJustReturn()
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    // MARK: - Table view delegate

    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .None
    }

}
