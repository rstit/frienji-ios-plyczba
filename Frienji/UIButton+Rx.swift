//
//  UIButton+Rx.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

import UIKit
import RxSwift
import RxCocoa

extension UIButton {

    var rx_image: AnyObserver<UIImage?> {
        return rx_image(.Normal)
    }

    func rx_image(controlState: UIControlState) -> AnyObserver<UIImage?> {
        return UIBindingObserver(UIElement: self) { button, image in
            button.setImage(image, forState: controlState)
        }.asObserver()
    }

    var rx_backgroundImage: AnyObserver<UIImage?> {
        return rx_backgroundImage(.Normal)
    }

    func rx_backgroundImage(controlState: UIControlState) -> AnyObserver<UIImage?> {
        return UIBindingObserver(UIElement: self) { button, image in
            button.setBackgroundImage(image, forState: controlState)
        }.asObserver()
    }

}
