//
//  CameraPermissionViewController.swift
//  Frienji
//
//  Created by bolek on 29.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class CameraPermissionViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "CameraPermissionViewController"
    
    //MARK: - View lefecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkCameraPermission()
    }
    
    private func checkCameraPermission() {
        PermissionsHelper.askForCameraPermission { (granded) in
            if(!granded) {
                 dispatch_async(dispatch_get_main_queue()) {
                    self.showCameraAccessRequiredAlert(true)
                }
            }
        }
    }
    
    private func showCameraAccessRequiredAlert(withAction:Bool) {
        if(withAction) {
            UIAlertUtils.showAlertWithTitle(Localizations.cameraPermission.alertTitle,
                                            message: Localizations.cameraPermission.alertMessage,
                                            fromController: self) {
                                                PermissionsHelper.openAppSettings()
            }
        } else {
            UIAlertUtils.showAlertWithTitle(Localizations.cameraPermission.alertTitle,
                                            message: Localizations.cameraPermission.alertMessage,
                                            fromController: self, showCompletion: nil)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func yesBtnClicked(sender:AnyObject){
        if(!PermissionsHelper.hasGrantedCameraPermission()) {
            self.showCameraAccessRequiredAlert(true)
            return
        }
    
        if(!PermissionsHelper.isRegisterdForPushNotifications()) {
            self.performSegueWithIdentifier("pushPermissionSegue", sender: nil)
        } else if(!PermissionsHelper.hasGrantedLocationPermission()) {
            self.performSegueWithIdentifier("locationPermissionSegue", sender: nil)
        } else if(Settings.sharedInstance.userVerified) {
            self.performSegueWithIdentifier("arSegue", sender: nil)
        } else {
            self.performSegueWithIdentifier("signInOrUpSegue", sender: nil)
        }
    }
    
    @IBAction func noBtnClicked(sender:AnyObject) {
        self.showCameraAccessRequiredAlert(false)
    }
}
