//
//  SignInOrUpViewController.swift
//  Frienji
//
//  Created by bolek on 01.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class SignInOrUpViewController: UIViewController, StoryboardLoad {
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "SignInOrUpViewController"

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "SignInSegue") {
            if let viewCont = segue.destinationViewController as? PhoneNumberViewController {
                viewCont.setEntryType(PhoneNumberViewEntryType.SignIn)
            }
        }
    }
}
