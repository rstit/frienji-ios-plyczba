//
//  WallTableViewCell.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxGesture

class PostCell: UITableViewCell {

    static let attachmentPlaceholder: UIImage = UIImage.emptyImageWithSize(CGSize(width: UIScreen.mainScreen().bounds.size.width, height: 70))

    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var like: UIButton!
    @IBOutlet weak var comment: UIButton!
    @IBOutlet weak var imageAttachment: UIImageView!
    @IBOutlet weak var locationAttachment: UIImageView!
    @IBOutlet weak var imageActivity: UIActivityIndicatorView!
    @IBOutlet weak var locationActivity: UIActivityIndicatorView!
    

    let needsUpdate = PublishSubject<Void>()
    var disposeBag = DisposeBag()

    func configure(withPost post: Post) {
        disposeBag = DisposeBag()
        let addToDisposeBag = { [unowned self] (disposable: Disposable) in disposable.addDisposableTo(self.disposeBag) }

        // View model
        let liked = like.rx_tap.filter(isUnliked)
        let unliked = like.rx_tap.filter(isLiked)
        let viewModel = PostViewModel.viewModel(
            postLoaded: Observable.just(post),
            liked: liked,
            unliked: unliked
        )

        // Bind view model
        [
            viewModel.map { $0.authorName } --> authorName.rx_text,
            viewModel.map { $0.createdAt } --> createdAt.rx_text,
            viewModel.map { $0.details } --> details.rx_text,
            viewModel.map { $0.avatar } --> avatar.rx_image,
            viewModel.map { $0.likeText } --> like.rx_title(),

        ].forEach(addToDisposeBag)

        // Load image attachment
        viewModel
            .map { $0.imageUrl }
            .flatMap { imageUrl -> Observable<UIImage?> in
                imageUrl.map { url in
                    url
                        .webImage
                        .map { $0.image }
                        .cachedImageForKey(url.absoluteString ?? "")
                        .startWith(PostCell.attachmentPlaceholder)
                } ?? .just(nil)
            }
            .doOnNext { [unowned self] in
                if $0 != PostCell.attachmentPlaceholder {
                    self.imageActivity.stopAnimating()
                }
            }
            .doOnError { [unowned self] _ in
                self.imageActivity.stopAnimating()
                // TODO: error alert
            }
            .bindTo(imageAttachment.rx_image)
            .addDisposableTo(disposeBag)
        
        // Show/hide image attachment in fullscreen
        imageAttachment.rx_gesture(.Tap)
            .subscribeNext { [unowned self] _ in
                self.imageAttachment.showFullscreen()
                self.imageAttachment.fullscreenImageView?.rx_gesture(.Tap)
                    .subscribeNext { [unowned self] _ in
                        self.imageAttachment.hideFullscreen()
                    }
                    .addDisposableTo(self.disposeBag)
            }
            .addDisposableTo(disposeBag)

        // Load location attachment
        viewModel
            .map { $0.location }
            .flatMap { location -> Observable<UIImage?> in
                location.map {
                    $0
                        .mapSnapshot
                        .map(Optional.init)
                        .cachedImageForLocation($0)
                        .startWith(PostCell.attachmentPlaceholder)
                } ?? .just(nil)
            }
            .doOnNext { [unowned self] in
                if $0 != PostCell.attachmentPlaceholder {
                    self.locationActivity.stopAnimating()
                }
            }
            .doOnError { [unowned self] _ in
                self.locationActivity.stopAnimating()
                // TODO: error alert
            }
            .bindTo(locationAttachment.rx_image)
            .addDisposableTo(disposeBag)
        
        // Show/hide location attachment fullscreen
        locationAttachment.rx_gesture(.Tap)
            .subscribeNext { [unowned self] _ in
                self.locationAttachment.showFullscreen()
                self.locationAttachment.fullscreenImageView?.rx_gesture(.Tap)
                    .subscribeNext { [unowned self] _ in
                        self.locationAttachment.hideFullscreen()
                    }
                    .addDisposableTo(self.disposeBag)
            }
            .addDisposableTo(disposeBag)

        // Disable like button
        [
            like.rx_tap.map { false } --> like.rx_enabled,
            like.rx_tap.map { 0.5 } --> like.rx_alpha,
        ].forEach(addToDisposeBag)

        // Enable like button
        [
            viewModel.map { _ in true } --> like.rx_enabled,
            viewModel.map { _ in 1.0 } --> like.rx_alpha,
        ].forEach(addToDisposeBag)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        locationAttachment.image = nil
        imageAttachment.image = nil
        locationActivity.startAnimating()
        imageActivity.startAnimating()
    }

    func isLiked() -> Bool {
        return like.titleLabel?.text == Localizations.wall.unlike
    }

    func isUnliked() -> Bool {
        return like.titleLabel?.text == Localizations.wall.like
    }

}
