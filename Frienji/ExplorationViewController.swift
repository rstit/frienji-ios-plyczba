//
//  ExplorationViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 07/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

struct ExplorationInput: Input {
    let catched = PublishSubject<Frienji>()
    let rejected = PublishSubject<Frienji>()
}

class ExplorationViewController: ArViewController, HasInput {

    // MARK: - Properties

    let kBackButtonOffset: CGFloat = -15.0
    let kNavigationBarFontSize: CGFloat = 17.0

    let darkBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        view.alpha = 0.5

        return view
    }()

    var input = ExplorationInput()

    var explorationViewModel: ExplorationViewModel!

    // MARK: - Outlets

    @IBOutlet weak var myWallButton: UIButton!
    @IBOutlet weak var activitiesButton: UIButton!

    let disposeBag = DisposeBag()

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        arOpenGLView.delegate = self
        configureNavigationBar()
        createBindings()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        navigationController?.navigationBarHidden = true
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Ar) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Ar) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Ar)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)

        navigationController?.navigationBarHidden = false
    }

    // MARK: - Configure

    func configureNavigationBar() {
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(kBackButtonOffset, 0), forBarMetrics: .Default)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.boldSystemFontOfSize(kNavigationBarFontSize)], forState: .Normal)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(kNavigationBarFontSize)]
    }

    // MARK: - Bindings

    func createBindings() {
        explorationViewModel = ExplorationViewModel(navigationHandler: NavigationHandler(sourceViewController: self))
        input.catched.bindTo(explorationViewModel.catched).addDisposableTo(disposeBag)
        input.rejected.bindTo(explorationViewModel.rejected).addDisposableTo(disposeBag)
        explorationViewModel.frienjisAround.asDriver().driveNext(arOpenGLView.updateWithTraces).addDisposableTo(disposeBag)

        // Dark background
        explorationViewModel.darkBackgroundVisible.asDriver()
            .driveNext { [unowned self] visible in self.switchDarkBackground(visible) }
            .addDisposableTo(disposeBag)

        // Remove bubble from AR
        Observable.of(input.catched, input.rejected).merge()
            .subscribeNext(arOpenGLView.removeTrace)
            .addDisposableTo(disposeBag)

        // Animate Zoo button
        input.catched
            .subscribeNext { [unowned self] _ in self.animateProfileIconAfterCatchingTrace() }
            .addDisposableTo(disposeBag)

        // Navigation
        myWallButton.rx_tap.bindTo(explorationViewModel.myWallTapped).addDisposableTo(disposeBag)
        activitiesButton.rx_tap.bindTo(explorationViewModel.activitiesTapped).addDisposableTo(disposeBag)
    }

    // MARK: - Exploration view controller

    func switchDarkBackground(visible: Bool) {
        if visible {
            darkBackground.frame = view.frame
            view.addSubview(darkBackground)
        } else {
            darkBackground.removeFromSuperview()
        }
    }

}

// MARK: - AR OpenGL view protocol

extension ExplorationViewController: ArOpenGLViewProtocol {

    func arOpenGLViewDidTouchTraceButIsTooFar(arOpenGLView: ArOpenGLView, trace: Frienji) {
        UIAlertUtils.showAlertWithTitle(Localizations.ar.trace_too_far(Int(trace.catchDistance)), fromController: self, showCompletion: nil)
    }

    func arOpenGLView(arOpenGLView: ArOpenGLView, didCatchTrace trace: Frienji) {
        explorationViewModel.previewTapped.onNext(trace)
    }

}
