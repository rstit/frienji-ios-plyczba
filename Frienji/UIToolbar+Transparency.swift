//
//  UIToolBar+Transparency.swift
//  Frienji
//
//  Created by Piotr Łyczba on 08/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

extension UIToolbar {
    
    func setTransparent(transparent: Bool, forToolbarPosition toolbarPosition: UIBarPosition = .Any, barMetrics: UIBarMetrics = .Default) {
        let image: UIImage? = transparent ? UIImage() : nil
        setBackgroundImage(image, forToolbarPosition: toolbarPosition, barMetrics: barMetrics)
        setShadowImage(image, forToolbarPosition: toolbarPosition)
    }
    
}
