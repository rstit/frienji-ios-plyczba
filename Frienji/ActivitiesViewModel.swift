//
//  ActivitiesViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

class ActivitiesViewModel {

    // MARK: - Outputs

    var activities = Variable<[Activity<Any>]>([])
    let apiError = PublishSubject<Void>()

    // MARK: - Inputs

    let loadInitialized = PublishSubject<Void>()
    let dismissTapped = PublishSubject<Void>()
    let loadMoreTriggered = PublishSubject<Void>()
    let activitySelected = PublishSubject<Activity<Any>>()

    // MARK: - Properties

    private let disposeBag = DisposeBag()

    private let api: FrienjiApi
    private let navigationHandler: NavigationHandler

    init(navigationHandler: NavigationHandler, api: FrienjiApi = FrienjiApi.sharedInstance) {
        self.api = api
        self.navigationHandler = navigationHandler

        prepareLoad()
        prepareNavigation()
    }

    func prepareLoad() {
        loadInitialized
            .flatMap { [unowned self] () -> Observable<[Activity<Any>]> in
                self.api.getActivities().doOnError { self.apiError.onError($0) }
            }
            .bindTo(activities)
            .addDisposableTo(disposeBag)
    }

    func prepareNavigation() {
        dismissTapped
            .bindTo(navigationHandler.modalDismissed(ExplorationViewController.self))
            .addDisposableTo(disposeBag)
    }

}
