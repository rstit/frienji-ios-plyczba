//
//  UIImageView+Fullscreen.swift
//  Frienji
//
//  Created by osx on 08/11/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    private static let kFullscreenImageViewTag = 30755

    var fullscreenImageView: UIImageView? {
        guard let window = UIApplication.sharedApplication().windows.last else {
            return nil
        }
        if let existingFullscreen = window.viewWithTag(UIImageView.kFullscreenImageViewTag) as? UIImageView {
            return existingFullscreen
        }
        
        return createFullscreenImageView()
    }
    
    private func createFullscreenImageView() -> UIImageView {
        let tmpImageView = UIImageView(frame: self.frame)
        tmpImageView.image = self.image
        tmpImageView.contentMode = UIViewContentMode.ScaleAspectFit
        tmpImageView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
        tmpImageView.tag = UIImageView.kFullscreenImageViewTag
        tmpImageView.alpha = 0.0
        tmpImageView.userInteractionEnabled = true
        
        return tmpImageView
    }
    
    private func createCloseLabel() -> UILabel {
        let label = UILabel(frame: CGRectZero)
        label.text = Localizations.attachment.close
        label.font = UIFont(name: "HelveticaNeue", size: 12.0)
        label.sizeToFit()
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor(white: 0.85, alpha: 1)
        label.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.8)
        label.alpha = 0.0
        
        return label
    }
    
    func showFullscreen() {
        guard let window = UIApplication.sharedApplication().windows.last
            where window.viewWithTag(UIImageView.kFullscreenImageViewTag) == nil else {
            return
        }
        guard let fullscreenImageView = fullscreenImageView else {
            return
        }
        
        let closeLabel = createCloseLabel()
        
        let labelWidth = window.frame.size.width
        let labelHeight = closeLabel.frame.size.height + 16
        closeLabel.frame = CGRectMake(0, window.frame.size.height - labelHeight, labelWidth, labelHeight)
        fullscreenImageView.addSubview(closeLabel)
        
        window.addSubview(fullscreenImageView)
        UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations: {
            fullscreenImageView.frame = window.frame
            fullscreenImageView.alpha = 1
            fullscreenImageView.layoutSubviews()
            closeLabel.alpha = 1
        }, completion: nil)
    }
    
    func hideFullscreen() {
        guard let fullscreenImageView = fullscreenImageView else {
            return
        }
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseInOut, animations: {
            fullscreenImageView.frame = self.frame
            fullscreenImageView.alpha = 0
            }, completion: { finished in
                fullscreenImageView.removeFromSuperview()
        })
    }

}
