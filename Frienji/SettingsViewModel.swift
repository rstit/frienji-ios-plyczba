//
//  SettingsViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 27/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Curry

class SettingsViewModel {

    let kContactUsUrl: NSURL! = NSURL(string: "http://www.frienji.io/contact")
    let kTermsAndConditionUrl: NSURL! = NSURL(string: "http://www.frienji.io/terms")

    let kNameValidationMaxCharactersCount = 20
    let kBioValidationMaxCharactersCount = 140

    // MARK: - Outputs

    var name = Variable<String>("")
    var bio = Variable<String>("")
    var avatar = Variable<UIImage?>(nil)
    var cover = Variable<UIImage?>(nil)

    let nameIsValid = Variable<Bool>(true)
    let bioIsValid = Variable<Bool>(true)
    let isValid: Observable<Bool>
    let error = PublishSubject<Void>()

    // MARK: - Inputs

    let initialized = PublishSubject<Void>()
    let avatarTapped = PublishSubject<Void>()
    let avatarSelected = BehaviorSubject<Avatar?>(value: nil)
    let saved = PublishSubject<Void>()
    let logoutSelected = PublishSubject<Void>()
    let termsAndConditionSelected = PublishSubject<Void>()
    let contactUsSelected = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    // MARK: - Dependencies

    private let navigation: NavigationHandler
    private let settings = Settings.sharedInstance
    private let api = FrienjiApi.sharedInstance

    // MARK: - Initialization

    init(navigationHandler: NavigationHandler, attachmentHandler: AttachmentHandler<SettingsViewController>) {
        navigation = navigationHandler
        isValid = Observable.combineLatest(
            nameIsValid.asObservable(),
            bioIsValid.asObservable()
        ) { $0 && $1 }

        prepareLoad()
        prepareSave()
        prepareContactUs()
        prepareTermsAndCondition()
        prepareLogout()
        prepareAvatarSelection()
        prepareCoverSelection(attachmentHandler.image)
        prepareValidation()
    }

    func prepareLoad() {
        let profile = initialized
            .flatMap { [unowned self] in
                self.settings.userFrienji.map(Observable.just) ?? .empty()
            }
            .shareReplay(1)

        profile
            .map { $0.username }
            .bindTo(name)
            .addDisposableTo(disposeBag)
        profile
            .flatMap { $0.whoAreYou.map(Observable.just) ?? .empty() }
            .bindTo(bio)
            .addDisposableTo(disposeBag)
        profile
            .map { $0.avatar.avatarImage }
            .bindTo(avatar)
            .addDisposableTo(disposeBag)
        profile
            .map { $0.backgroundImageUrl.flatMap(NSURL.init) }
            .flatMap { [unowned self] url -> Observable<ImageLoad> in
                url?.webImage
                    .doOnError { self.error.onError($0) }
                ?? .empty()
            }
            .map { $0.image ?? UIImage() }
            .bindTo(cover)
            .addDisposableTo(disposeBag)
    }

    func prepareSave() {
        saved
            .map { [unowned self] in curry(self.api.updateProfile) }
            .withLatestFrom(name.asObservable()) { $0($1) }
            .withLatestFrom(bio.asObservable()) { $0($1) }
            .withLatestFrom(avatarSelected) { $0($1) }
            .withLatestFrom(cover.asObservable()) { $0($1) }
            .flatMap { [unowned self] updateProfile -> Observable<Frienji?> in
                updateProfile
                    .map(Optional.init)
                    .doOnError { self.error.onError($0) }
                    .catchErrorJustReturn(nil)
            }
            .bindTo(navigation.popped(WallViewController.self)) { input, user in
                if let user = user {
                    input.loaded.onNext(user)
                }
            }
            .addDisposableTo(disposeBag)
    }

    func prepareLogout() {
        logoutSelected
            .flatMap { [unowned self] in
                self.api.logout()
            }
            .subscribeNext { [unowned self] in
                self.navigation.replaceRoot(IntroductionViewController.self)
            }
            .addDisposableTo(disposeBag)
    }

    func prepareTermsAndCondition() {
        termsAndConditionSelected
            .subscribeNext { [unowned self] in
                self.navigation.goToWebsite(self.kTermsAndConditionUrl)
            }
            .addDisposableTo(disposeBag)
    }

    func prepareContactUs() {
        contactUsSelected
            .subscribeNext { [unowned self] in
                self.navigation.goToWebsite(self.kContactUsUrl)
            }
            .addDisposableTo(disposeBag)
    }

    func prepareAvatarSelection() {
        avatarTapped
            .subscribeNext { [unowned self] in
                self.navigation.presentModal(AvatarGroupViewController.self)
            }
            .addDisposableTo(disposeBag)
        avatarSelected
            .map { $0?.avatarImage }
            .bindTo(avatar)
            .addDisposableTo(disposeBag)
    }

    func prepareCoverSelection(coverSelected: Observable<UIImage?>) {
        coverSelected
            .bindTo(cover)
            .addDisposableTo(disposeBag)
    }

    func prepareValidation() {
        name.asObservable()
            .map {
                $0.characters.count < self.kNameValidationMaxCharactersCount
            }
            .bindTo(nameIsValid)
            .addDisposableTo(disposeBag)
        bio.asObservable()
            .map {
                $0.characters.count < self.kBioValidationMaxCharactersCount
            }
            .bindTo(bioIsValid)
            .addDisposableTo(disposeBag)
    }

}
