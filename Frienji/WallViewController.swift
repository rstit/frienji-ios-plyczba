//
//  WallViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation
import JSQMessagesViewController

struct WallInput: Input {
    let loaded = ReplaySubject<Frienji>.create(bufferSize: 1)
}

class WallViewController: PostsViewController, UITableViewDelegate, HasInput, HasDisposeBag {

    // MARK: - Subviews

    // MARK: - Observables

    var input = WallInput()
    let textSent = PublishSubject<String>()
    let postCommented = PublishSubject<Post>()
    var attachmentHandler: AttachmentHandler<WallViewController>!
    var viewModel: Observable<WallViewModel> {
        return Observable.empty()
    }
    let disposeBag = DisposeBag()

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        attachmentHandler = AttachmentHandler(targetViewController: self)

        configureView()
        createBindings()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        updateTableViewInsets()
        
        switch (self) {
            
        case is WallPostsViewController:
            if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.FrienjiWall) {
                let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.FrienjiWall) {
                    Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.FrienjiWall)
                }
                coachmark.showWithAnimationInContainer()
            }
            
        default:
            if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.UserWall) {
                let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.UserWall) {
                    Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.UserWall)
                }
                coachmark.showWithAnimationInContainer()
            }
        }
    }

    // MARK: - Bindings

    func createBindings() {

        // Image attachments
        attachmentHandler.image
            .bindTo(inputToolbar.rx_imageAttachment)
            .addDisposableTo(disposeBag)

        // Location attachment
        attachmentHandler.location
            .flatMap { $0?.mapSnapshot.map(Optional.init) ?? Observable.just(nil) }
            .bindTo(inputToolbar.rx_locationAttachment)
            .addDisposableTo(disposeBag)

        let viewModel = self.viewModel.showAlertOnApiError(self)

        // Bind profile
        viewModel
            .map { $0.frienjiName }
            .bindTo(rx_title)
            .addDisposableTo(disposeBag)

        // Bind posts
        viewModel
            .map { $0.posts }
            .bindTo(tableView.rx_itemsWithCellIdentifier(R.reuseIdentifier.wallPostCell.identifier)) { [unowned self] (index, post, cell: PostCell) in
                cell.configure(withPost: post)
                cell.comment.rx_tap
                    .subscribeNext { [unowned self] in
                        self.postCommented.onNext(post)
                    }
                    .addDisposableTo(cell.disposeBag)
                cell.needsUpdate
                    .subscribeNext { [unowned self] in
                        UIView.setAnimationsEnabled(false)
                        self.tableView.beginUpdates()
                        self.tableView.endUpdates()
                        UIView.setAnimationsEnabled(true)
                    }
                    .addDisposableTo(cell.disposeBag)
            }
            .addDisposableTo(disposeBag)
    }

    // MARK: - Wall view controller

    func configureView() {
        inputToolbar.contentView.rightBarButtonItem.setTitleColor(UIColor.appPurpleColor(), forState: .Normal)
        inputToolbar.contentView.textView.font = ChatViewController.kSystemFont

        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        sizeTableHeaderViewToFit()
    }

    func sizeTableHeaderViewToFit() {
        guard let headerView = tableView.tableHeaderView else {
            return
        }

        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = headerView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
        headerView.frame.size.height = height
    }

    override func didPressSendButton(sender: UIButton, withText text: String, date: NSDate) {
        textSent.onNext(text)
        finishSendingMessageAnimated(true)
    }

    override func didPressAccessoryButton(sender: UIButton) {
        // JSQMessageViewController requires this to be implemented
    }

    // MARK: - Table view delegate

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.wallSpaceCell)
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.wallSpaceCell)?.bounds.height ?? tableView.sectionHeaderHeight
    }

    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return .None
    }

}

extension WallViewController: HasAttachment {

    var attachmentRequested: Observable<Void> {
        return inputToolbar.contentView.leftBarButtonItem.rx_tap.asObservable()
    }

}
